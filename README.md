# SCDAQ
Rudimentary data taking application for the CMS L1 scouting demonstrator

An RPM is built automatically by the CI pipeline and is stored as artifact of the "build" stage. **Important:** Once installed, the SCDAQ config file needs to be modified as described in the section below.

The RPM installs a systemd service that can be started with

```bash
$ sudo systemctl start runSCdaq
```

if SCDAQ should be automatically started at boot it can be enabled with

```bash
$ sudo systemctl enable runSCdaq
```

## Developing

We try to adhere to the [Google C++ style guide](https://google.github.io/styleguide/cppguide.html).

### Code formatting

The C++ code needs to follow the directives set forth in the `.clang-format` file placed in the repository root. This is enforced by the "check" stage of the CI.

You can format your code either with `clang-format -i [modified file]`, or (in particular for multiple changed files) with `git clang-format` after you have staged the modified files.

## Manual operation

### Build manually

This project uses the CMake meta build system (`cmake`). This generates a Makefile (`make`) which is used to build the target executable (`scdaq`).

**Use supplied CMakeLists.txt to generate Makefile and build:**

```
$ mkdir -p build  # Create a separate build directory if one does not yet exist
$ cmake ..        # Generate Makefile from the CMakeLists.txt in the root directory
$ make scdaq      # Build SCDAQ from the generated Makefile in the build directory
```

### Run manually

1. Start the run control on `scoutdaq`:
    ```
    $ cd scripts
    $ ./runControl.py
    ```

1. Start the data taking application on `scoutdaq`:
    ```
    $ cd scripts
    $ ./runSCdaq.sh -- config /etc/scdaq/scdaq.conf
    ```

## Configuration

By default the configuration file in `/etc/scdaq/scdaq.conf` is used. Examples of configuration files for various systems can be found in `/opt/scdaq/test/config/`.

Currently, one configuration file is assigned to each stream in the case of many parallel processing pipelines (input to output).

**Use case 1: single stream**

In the simplest use case, SCDAQ instantiates a single pipeline to process one input source into an output sink, defined in a single configuration file. It is advised to call the executable with the `--config` argument (containing the relative configuration file path and name) and to not specify `--nstreams`, since the latter argument currently alters the ultimate resulting filename indicated by the former (see use case 2).

```
./build/scdaq --config example.conf
```

**Use case 2: multiple streams**

For `N` parallel streams, `N` configuration files must be provided. This is currently supported by specifying the relative path and "base" name of the configuration files through `--config` and the number of streams through `--nstreams`. The resulting configuration file paths are formed by combining the information passed through both arguments.

Example: to run three parallel streams, supply three configuration files to `scdaq`. These files must share the same parent directory and a common base name, suffixed by a strictly increasing integer between 1 and the value indicated by `--nstreams`. Then, run the executable with:

```
./build/scdaq --config etc/scdaq/example.conf --nstreams 3
```

This command prompts `scdaq` to consult the configuration files `etc/scdaq/example1.conf`, `etc/scdaq/example2.conf` and `etc/scdaq/example3.conf` (but NOT `etc/scdaq/example.conf`) to instantiate the pipelines.


## Compatibility

The MicronDMA option is designed for use with picocomputing-2021.2.7.

