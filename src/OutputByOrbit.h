#ifndef OUTPUTBYORBIT_H
#define OUTPUTBYORBIT_H

#include <tbb/pipeline.h>

#include <cstdint>
#include <cstdio>
#include <string>

#include "OutputFileHandler.h"
#include "slice.h"

class ctrl;
class Config;
class ConfigView;

//! Filter that writes each buffer to a file.
class OutputByOrbitStream : public tbb::filter {
 public:
  OutputByOrbitStream(ctrl &c, const ConfigView &conf_view);
  ~OutputByOrbitStream();
  void *operator()(void *item) override;
  void OutputFixedOrbits(Slice &out);

 private:
  void MonitorEndOfRun();
  std::thread monitoring_thread_;

  // used for fixed N orbits per file approach
  uint64_t totcounts_;
  ctrl &control_;
  const ConfigView &conf_view_;
  const Config &conf_;
  OutputFileHandler output_file_handler_;
  size_t min_buffer_queue_size_;
};

#endif
