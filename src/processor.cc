#include "processor.h"

#include <iomanip>

#include "log.h"
#include "slice.h"

// Definition of the static member stats
Processor::Statistics Processor::stats;

Processor::Processor() : tbb::filter(parallel) {
  LOG(TRACE) << "Created transform filter at " << static_cast<void *>(this);
}

void *Processor::operator()(void *item) {
  Slice &input = *static_cast<Slice *>(item);
  Slice &out = *Slice::getAllocated();
  ProcessSlice(input, out);
  Slice::giveAllocated(&input);

  return &out;
}

void Processor::ProcessSliceImpl(Slice &input, Slice &out) {
  char *rd_ptr = input.begin();
  char *wr_ptr = out.begin();

  // Passthrough
  memcpy(wr_ptr, rd_ptr, input.size());
  out.set_end(out.begin() + input.size());
  out.set_counts(1);

  LOG(TRACE) << "Passthrough processor copied " << input.size() << " bytes";
  return;
}

void Processor::ProcessSlice(Slice &input, Slice &out) {
  nbPackets++;
  stats.packet_count++;

  ProcessSliceImpl(input, out);
}
