#include "ugt_orbit_processor.h"

#include <cassert>

int UgtOrbitProcessor::ProcessBlock(MemRegion &readable_block, WriteMemRegion &writeable_block,
                                    BxMetadata meta) {
  struct BxMetadataType {
    uint32_t bx;
    int orbit;
  };

  using SourceDataType = blockUgt;

  assert(readable_block.CheckBounds(sizeof(SourceDataType)));
  writeable_block.Fill(BxMetadataType{meta.bx, meta.orbit});

  memcpy(writeable_block.Scan<SourceDataType>(), readable_block.Scan<SourceDataType>(),
         sizeof(SourceDataType));

  return 1;
}