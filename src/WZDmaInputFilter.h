#ifndef WZDMA_INPUT_FILTER_H
#define WZDMA_INPUT_FILTER_H

#include <tbb/pipeline.h>
#include <tbb/tick_count.h>

#include <iostream>
#include <memory>

#include "InputFilter.h"
#include "config.h"
#include "wzdma/wz_dma.h"

class WZDmaInputFilter : public InputFilter {
 public:
  WZDmaInputFilter(size_t packetBufferSize, size_t nbPacketBuffers, ctrl &control, SconeAddress);
  ~WZDmaInputFilter() override;

 protected:
  ssize_t readInput(char **buffer, size_t bufferSize) override;
  void readComplete(char *buffer) override;
  void print(std::ostream &out) const override;

 private:
  ssize_t read_packet_from_dma(char **buffer);
  ssize_t read_packet(char **buffer, size_t bufferSize);

  struct Statistics {
    uint64_t nbDmaErrors = 0;
    uint64_t nbDmaOversizedPackets = 0;
    uint64_t nbBoardResets = 0;
  } stats;

  struct wz_private dma_;
  bool dma_is_initialized_{};

  SconeAddress scone_address_;
};

#endif  // WZDMA_INPUT_FILTER_H
