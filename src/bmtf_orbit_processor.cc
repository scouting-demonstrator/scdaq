#include "bmtf_orbit_processor.h"

#include <cassert>

#include "log.h"

int BmtfOrbitProcessor::ProcessBlock(MemRegion &readable_block, WriteMemRegion &writeable_block,
                                     BxMetadata meta) {
  using SourceDataType = blockBmtf;
  using SinkDataType = uint64_t;
  const uint32_t source_data_length = 8;

  assert(readable_block.CheckBounds(sizeof(SourceDataType)));
  SourceDataType *bl = readable_block.Scan<SourceDataType>();

  BxData<SinkDataType, source_data_length> bx_data;
  for (unsigned int i = 0; i < source_data_length; i++) {
    if ((bl->stub[i] >> shiftsBmtfStub::valid) & masksBmtfStub::valid) {
      bx_data.Add(bl->stub[i]);
    }
  }

  if (bx_data.count == 0) {
    LOG(WARNING) << '#' << nbPackets
                 << ": Detected a bx with zero stubs, this should not happen. Packet is skipped.";
    return -1;
  }

  // header word of packed bmtf stub data contains count of stubs in bx
  // as well as the warning test enable flag.
  meta.header = uint32_t{(static_cast<uint32_t>(meta.header) << 8) + bx_data.count};
  writeable_block.Fill(meta);
  writeable_block.Fill(bx_data.GetBuffer(), bx_data.GetSize());

  return bx_data.count;
}