#include "calo_orbit_processor.h"

#include <cassert>

int CaloOrbitProcessor::ProcessBlock(MemRegion &readable_block, WriteMemRegion &writeable_block,
                                     BxMetadata meta) {
  struct CaloLinkData {
    uint32_t id;
    uint32_t data[6];  // Contains inverse matrix `calo0..6[id]` from `blockCalo`
  };

  using SourceDataType = blockCalo;
  using SinkDataType = CaloLinkData;
  const uint32_t source_data_length = 8;

  assert(readable_block.CheckBounds(sizeof(SourceDataType)));
  SourceDataType *bl = readable_block.Scan<SourceDataType>();

  BxData<SinkDataType, source_data_length> bx_data;
  for (uint32_t i = 0; i < source_data_length; i++) {
    bx_data.Add(
        {i, bl->calo0[i], bl->calo1[i], bl->calo2[i], bl->calo3[i], bl->calo4[i], bl->calo5[i]});
  }

  writeable_block.Fill(BxMetadata{meta.header, meta.bx, meta.orbit});
  writeable_block.Fill(bx_data.GetBuffer(), bx_data.GetSize());

  return 1;
}