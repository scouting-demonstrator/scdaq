#include "calo_obj_orbit_processor.h"

#include <cassert>

#include "log.h"

// Returns the counts if valid or a negative error code
int CaloObjOrbitProcessor::ProcessBlock(MemRegion &readable_block, WriteMemRegion &writeable_block,
                                        BxMetadata meta) {
  using SourceDataType = blockCaloObj;
  using SinkDataType = uint32_t;
  const uint32_t source_data_length = 12;

  assert(readable_block.CheckBounds(sizeof(SourceDataType)));
  SourceDataType *bl = readable_block.Scan<SourceDataType>();

  BxData<SinkDataType, source_data_length> bx_data;
  for (unsigned int i = 0; i < source_data_length; i++) {
    const auto et = uint32_t{(bl->calo_obj[i] >> shiftsCaloObjEt) & maskCaloObjEt};
    if ((et > 0) || (doZS == 0)) {
      bx_data.Add(bl->calo_obj[i]);
    }
  }

  if (bx_data.count == 0) {
    LOG(WARNING) << '#' << nbPackets
                 << ": Detected a bx with zero calo objects, this should not "
                    "happen. Packet is skipped.";
    return -1;
  }

  meta.header = uint32_t{(static_cast<uint32_t>(meta.header) << 8) + bx_data.count};
  writeable_block.Fill(meta);
  writeable_block.Fill(bx_data.GetBuffer(), bx_data.GetSize());

  return bx_data.count;
}
