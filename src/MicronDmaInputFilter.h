#ifndef MICRONDMA_INPUT_FILER_H
#define MICRONDMA_INPUT_FILER_H

#include <micron_dma.h>

#include <memory>

#include "InputFilter.h"
#include "config.h"

class MicronDmaInputFilter : public InputFilter {
 public:
  MicronDmaInputFilter(size_t, size_t, ctrl &, Config::ProcessorType p);
  ~MicronDmaInputFilter() override;

 protected:
  ssize_t readInput(char **buffer, size_t bufferSize) override;
  void print(std::ostream &out) const override;

 private:
  micron_private *pico_{};
  int stream2_;
  ssize_t runMicronDAQ(char **buffer);
  Config::ProcessorType processorType_;
};

#endif  // MICRONDMA_INPUT_FILER_H
