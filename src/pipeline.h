#ifndef PIPELINE_H
#define PIPELINE_H

#include <sysexits.h>

#include <boost/asio.hpp>
#include <boost/bind/bind.hpp>
#include <boost/thread.hpp>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <memory>

#include "DmaInputFilter.h"
#include "FileDmaInputFilter.h"
#include "InputFilter.h"
#include "MicronDmaInputFilter.h"
#include "OutputByOrbit.h"
#include "OutputBySize.h"
#include "TcpInputFilter.h"
#include "WZDmaInputFilter.h"
#include "config.h"
#include "controls.h"
#include "format.h"
#include "processor.h"
#include "server.h"
#include "tbb/concurrent_queue.h"
#include "tbb/pipeline.h"
#include "tbb/task_group.h"
#include "tbb/task_scheduler_init.h"
#include "tbb/tbb_allocator.h"
#include "tbb/tick_count.h"
#include "tools.h"

class Pipeline {
  friend class MultiPipeline;
  using OutputFilter = tbb::filter;

 public:
  static ctrl control_;
  const ConfigView conf_view_;        // Needed for multi-pipeline configuration contexts
  const uint max_tokens_per_thread_;  // Impacts the maximum number of in-flight TBB tokens

  std::shared_ptr<InputFilter> input_filter_;
  std::shared_ptr<Processor> stream_processor_;
  std::shared_ptr<OutputFilter> output_stream_;
  tbb::pipeline pipeline_;

  explicit Pipeline(ConfigView conf_view, uint max_tokens_per_thread = 4);
};

class MultiPipeline {
 public:
  const Config conf_;
  std::deque<Pipeline> pipelines_;

  tbb::task_scheduler_init scheduler_init_;
  boost::asio::io_service io_service_;
  boost::thread server_thread_;
  std::unique_ptr<server> server_;
  bool verbose_ = true;

  explicit MultiPipeline(Config c);
  int Run();

 private:
  int RunPipelines(uint num_streams);
};

#endif