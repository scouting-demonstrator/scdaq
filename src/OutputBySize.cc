#include "OutputBySize.h"

#include <cstdio>
#include <fstream>
#include <iostream>
#include <string>
#include <system_error>

#include "log.h"
#include "slice.h"
#include "tools.h"

/* Defines the journal file. Note: Filename prefix is added making the final
 * filename */
static const std::string journal_file{"index.journal"};

/* Defined where are the files stored before they are moved to the final
 * destination */
static const std::string working_dir{"in_progress"};

static void create_output_directory(std::string &output_directory) {
  struct stat sb;

  /* check if path exists and is a directory */
  if (stat(output_directory.c_str(), &sb) == 0) {
    if (S_ISDIR(sb.st_mode)) {
      LOG(TRACE) << "Output directory already exists: " << output_directory << "'.";
      return;
    }
    std::string err = "ERROR The output directory path '" + output_directory +
                      "' exists, but the path is not a directory!";
    LOG(ERROR) << err;
    throw std::runtime_error(err);
  }

  if (!tools::filesystem::create_directories(output_directory)) {
    std::string err =
        tools::strerror("ERROR when creating the output directory '" + output_directory + "'");
    LOG(ERROR) << err;
    throw std::runtime_error(err);
  }
  LOG(TRACE) << "Created output directory: " << output_directory << "'.";
}

OutputBySizeStream::OutputBySizeStream(std::string output_filename_base,
                                       std::string output_filename_prefix,
                                       std::string output_filename_suffix, ctrl &c,
                                       Config::ProcessorType processorType_)
    : tbb::filter(serial_in_order),
      output_filename_base_(std::move(output_filename_base)),
      output_filename_prefix_(std::move(output_filename_prefix)),
      output_filename_suffix_(std::move(output_filename_suffix)),
      totcounts(0),
      current_file_size(0),
      file_count(-1),
      control(c),
      current_file(nullptr),
      current_run_number(0),
      journal_name(output_filename_base_ + "/" + output_filename_prefix_ + '_' + journal_file),
      min_buffer_queue_size_(1000),
      bril_(processorType_ == Config::ProcessorType::BRIL) {
  LOG(TRACE) << "Created output filter at " << static_cast<void *>(this);

  // Create the ouput directory
  std::string output_directory = output_filename_base_ + "/" + working_dir;
  create_output_directory(output_directory);
}

static void update_journal(const std::string &journal_name, uint32_t run_number, uint32_t index) {
  std::string new_journal_name = journal_name + ".new";

  // Create a new journal file
  std::ofstream journal(new_journal_name);
  if (journal.is_open()) {
    journal << run_number << "\n" << index << "\n";
    journal.close();
  } else {
    LOG(ERROR) << "WARNING: Unable to open journal file";
  }

  // Replace the old journal
  if (rename(new_journal_name.c_str(), journal_name.c_str()) < 0) {
    LOG(ERROR) << tools::strerror("Journal file move failed");
  }
}

static bool read_journal(const std::string &journal_name, uint32_t &run_number, uint32_t &index) {
  std::ifstream journal(journal_name);
  if (journal.is_open()) {
    journal >> run_number >> index;
    journal.close();
    return true;
  }
  return false;
}

void *OutputBySizeStream::operator()(void *item) {
  Slice &out = *static_cast<Slice *>(item);
  totcounts += out.get_counts();

  if (control.running.load(std::memory_order_acquire) || control.output_force_write) {
    if (current_file == nullptr || current_file_size > control.max_file_size ||
        current_run_number != control.run_number || bril_) {
      open_next_file();
    }

    size_t n = fwrite(out.begin(), 1, out.size(), current_file);
    current_file_size += n;
    if (n != out.size()) {
      LOG(ERROR) << "Can't write into output file: Have to write " << out.size()
                 << ", but write returned " << n;
    }
  }

  // If not running and we have a file then close it
  if (!control.running && current_file != nullptr && !control.output_force_write) {
    close_and_move_current_file();
    file_count = -1;
  }
  if (Slice::current_queue_size() < min_buffer_queue_size_) {
    LOG(TRACE) << "New minimum queue size is " << Slice::current_queue_size() << " total counts "
               << totcounts;
    min_buffer_queue_size_ = Slice::current_queue_size();
  }

  Slice::giveAllocated(&out);

  return nullptr;
}

static std::string format_run_file_stem(const std::string &filename_prefix,
                                        const std::string &filename_suffix, uint32_t run_number,
                                        int32_t file_count) {
  std::ostringstream oss;
  // output filename formatting: run number and file count are headed by up to six zeros
  oss << filename_prefix << "_" << std::setw(6) << std::setfill('0') << run_number << "_"
      << std::setw(6) << std::setfill('0') << file_count << filename_suffix;
  return oss.str();
}

void OutputBySizeStream::close_and_move_current_file() {
  // Close and move current file
  if (current_file) {
    fclose(current_file);
    current_file = nullptr;

    std::string run_file = format_run_file_stem(output_filename_prefix_, output_filename_suffix_,
                                                current_run_number, file_count);
    std::string current_file_name = output_filename_base_ + "/" + working_dir + "/" + run_file;
    std::string target_file_name = output_filename_base_ + "/" + run_file;

    LOG(INFO) << "rename: " << current_file_name << " to " << target_file_name;
    if (rename(current_file_name.c_str(), target_file_name.c_str()) < 0) {
      LOG(ERROR) << tools::strerror("File rename failed");
    }

    current_file_size = 0;
    file_count += 1;
  }
}

void OutputBySizeStream::open_next_file() {
  close_and_move_current_file();

  // We can change current_run_number only here, after the (previous) file was
  // closed and moved
  if (current_run_number != control.run_number) {
    current_run_number = control.run_number;
    file_count = -1;
  }

  // If this is the first file then check if we have a journal file
  if (file_count < 0) {
    // Set default file index
    file_count = 0;

    uint32_t journal_run_number;
    uint32_t index;

    if (read_journal(journal_name, journal_run_number, index)) {
      LOG(INFO) << "We have journal:";
      LOG(INFO) << "  run_number: " << journal_run_number;
      LOG(INFO) << "  index:      " << index;
    } else {
      LOG(INFO) << "No journal file.\n";
    }

    LOG(INFO) << "Current run_number: " << current_run_number;
    if (current_run_number == journal_run_number) {
      file_count = static_cast<int>(index);
    }
    LOG(INFO) << "  using index:      " << file_count;
  }

  // Create the output directory
  std::string output_directory = output_filename_base_ + "/" + working_dir;
  create_output_directory(output_directory);

  // Create a new file
  std::string current_filename =
      output_directory + "/" +
      format_run_file_stem(output_filename_prefix_, output_filename_suffix_, current_run_number,
                           file_count);
  current_file = fopen(current_filename.c_str(), "w");
  if (current_file == nullptr) {
    std::string err = tools::strerror("ERROR when creating file '" + current_filename + "'");
    LOG(ERROR) << err;
    throw std::runtime_error(err);
  }

  // Update journal file (with the next index file)
  update_journal(journal_name, current_run_number, file_count + 1);
}
