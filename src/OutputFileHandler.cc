#include "OutputFileHandler.h"

#include <tbb/concurrent_queue.h>

#include <atomic>
#include <cassert>
#include <cstdio>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <string>

#include "OutputFile.h"
#include "log.h"
#include "tools.h"

const std::string OutputFileHandler::working_dir_ = "in_progress";
const std::string OutputFileHandler::journal_file_ = "index.journal";

tbb::concurrent_bounded_queue<OutputFile> OutputFileHandler::files_to_close_ =
    tbb::concurrent_bounded_queue<OutputFile>();
std::atomic<bool> OutputFileHandler::file_handler_running_{true};

void OutputFileHandler::enqueue_current_file_for_close_and_move_maybe() {
  if (outputFile_.exists()) {
    LOG(TRACE) << "queueing file: " << outputFile_.getFileName() << " for closing, queue size now "
               << files_to_close_.size();
    if (this->OutputFileHandler::getCMSSWHeaders()) {
      outputFile_.setFileHeader(createFileHeader(outputFile_.Lumisection()));
      outputFile_.writeFileHeader();
      outputFile_.setFileSize(file_size_);
      outputFile_.setNOrbits(NOrbits_);
    }
    if (outputFile_.getFileHeader().getFileSize() > 32) {
      files_to_close_.push(outputFile_);
      LOG(TRACE) << "queued " << outputFile_.getFileName() << " queue size now "
                 << files_to_close_.size();
    } else {
      fclose(outputFile_.getFilePtr());
    }
    // resetting for next file
    NOrbits_ = 0;
    if (this->OutputFileHandler::getCMSSWHeaders()) {
      file_size_ = sizeof(FRDFileHeader_v2);
    } else {
      file_size_ = 0;
    }
    outputFile_.nullify();
  }
}

int OutputFileHandler::GetCurrentRunNumber() const { return current_run_number_; }

void OutputFileHandler::UpdateRunInfo(uint32_t run, uint32_t index) {
  if (current_run_number_ != static_cast<int>(run)) {
    run_NOrbits_ = 0;
    run_NFiles_ = 0;
    LOG(TRACE) << "Previous run: " << current_run_number_ << " | New run: " << run;
  }

  if (current_index_ != static_cast<int>(index)) {
    LOG(TRACE) << "Previous index: " << current_index_ << " | New index: " << index;
  }

  current_index_ = static_cast<int>(index);
  current_run_number_ = static_cast<int>(run);
  std::stringstream path_ss;
  path_ss << base_path_ << "/run" << std::setfill('0') << std::setw(6) << current_run_number_;
  run_dir_ = path_ss.str();
  path_ss << "/" << working_dir_;
  working_files_base_path_ = path_ss.str();
}

bool OutputFileHandler::MaybeCommitFile(uint32_t run, uint32_t index) {
  bool is_new_run = (current_run_number_ != static_cast<int>(run));
  bool is_new_index = (current_index_ != static_cast<int>(index));

  //  // TODO: We should maybe move this out of this function...
  //  if (current_run_number_ > 0 && is_new_run && getCMSSWHeaders()) {
  //    OutputFileHandler::write_EoR_file();
  //  }

  if (is_new_run || is_new_index) {
    enqueue_current_file_for_close_and_move_maybe();
    UpdateRunInfo(run, index);
    return true;
  }
  return false;
}

OutputFile OutputFileHandler::getFile(uint32_t run, uint32_t index) {
  if (MaybeCommitFile(run, index)) {
    open_new_file();
  }
  return outputFile_;
}

FRDFileHeader_v2 OutputFileHandler::createFileHeader(uint32_t ls) {  // file header for CMSSW input
  uint16_t headerSize = sizeof(FRDFileHeader_v2);
  uint16_t dataType = static_cast<uint16_t>(sourceID_);
  uint32_t eventCount = NOrbits_;
  uint32_t runNumber = current_run_number_;
  uint64_t fileSize = file_size_;
  FRDFileHeader_v2 fileHeader(headerSize, dataType, eventCount, runNumber, ls, fileSize);
  return fileHeader;
}

void OutputFileHandler::create_output_directory_maybe(std::string& output_directory) {
  struct stat sb;
  LOG(TRACE) << "checking if working directory " << output_directory << " exists ";
  /* check if path exists and is a directory */
  if (stat(output_directory.c_str(), &sb) == 0) {
    if (S_ISDIR(sb.st_mode)) {  // output directory already exists
      return;
    }
    std::string err = "ERROR The output directory path '" + output_directory +
                      "' exists, but the path is not a directory!";
    LOG(ERROR) << err;
    throw std::runtime_error(err);
  }

  if (!tools::filesystem::create_directories(output_directory)) {
    std::string err =
        tools::strerror("ERROR when creating the output directory '" + output_directory + "'");
    LOG(ERROR) << err;
    throw std::runtime_error(err);
  }
  LOG(TRACE) << "Created output directory: " << output_directory << "'.";
}

void OutputFileHandler::open_new_file() {
  // Create a new file
  uint32_t ls = 1 + static_cast<uint32_t>(current_index_ / (max_index_per_ls_ + 1));
  create_output_directory_maybe(working_files_base_path_);
  std::string filename = format_filename(current_run_number_, current_index_, ls);
  std::string full_filename = working_files_base_path_ + "/" + filename;
  LOG(TRACE) << "opening file with index " << current_index_ << ", in lumisection " << ls;
  OutputFile outputFile(fopen(full_filename.c_str(), "wbx"), filename, createFileHeader(ls), ls,
                        current_index_ % (max_index_per_ls_ + 1),
                        run_dir_);  // TODO: run_dir_ probably needs also the run number
  if (!outputFile.exists()) {
    std::string err =
        tools::strerror("ERROR when creating file '" + outputFile.getFileName() + "'");
    LOG(ERROR) << err;
    throw std::runtime_error(err);
  }
  outputFile_ = outputFile;
  // reserve space for CMSSW header if required
  if (this->OutputFileHandler::getCMSSWHeaders()) {
    fseek(outputFile_.getFilePtr(), sizeof(FRDFileHeader_v2), SEEK_SET);
  }
}

// Create a properly formatted file name
std::string OutputFileHandler::format_filename(uint32_t run_number, uint32_t index, uint32_t ls) {
  std::ostringstream ofilename;

  if (this->OutputFileHandler::getCMSSWHeaders()) {
    ofilename << "run" << std::setfill('0') << std::setw(6) << run_number << "_ls"
              << std::setfill('0') << std::setw(4) << ls << "_index" << std::setfill('0')
              << std::setw(6) << index % (max_index_per_ls_ + 1) << filename_suffix_;

  } else {
    ofilename << filename_prefix_ << "_" << std::setfill('0') << std::setw(6) << run_number << "_"
              << std::setfill('0') << std::setw(6) << index << filename_suffix_;
  }
  return ofilename.str();
}

void OutputFileHandler::close_and_rename::operator()() const {
  OutputFile outputFile;
  while (file_handler_running_ || (files_to_close_.size() > 0)) {
    LOG(TRACE) << "try pop now. queue size now " << files_to_close_.size();
    try {
      files_to_close_.pop(outputFile);
    } catch (tbb::user_abort& e) {
      LOG(ERROR) << "Exception caught when trying to pop files to close queue.";
      break;
    }
    LOG(TRACE) << "popping file: " << outputFile.getFileName() << " for closing, queue size now "
               << files_to_close_.size();
    if (fclose(outputFile.getFilePtr()) != 0) {
      LOG(ERROR) << tools::strerror("File close failed");
    }
    std::string from = outputFile.getRunDir() + "/" + working_dir_ + "/" + outputFile.getFileName();
    std::string to = outputFile.getRunDir() + "/" + outputFile.getFileName();
    if (rename(from.c_str(), to.c_str()) != 0) {
      LOG(ERROR) << tools::strerror("File rename of " + outputFile.getFileName() + " failed");
    }

    // TODO: new counters logic here to be tested
    outputfilehandler_->updateLumisectionFileSize(outputFile.getFileSize());
    outputfilehandler_->updateLumisectionNOrbits(outputFile.getNOrbits());
    outputfilehandler_->updateLumisectionNFiles(1);

    if (outputFile.Index() == outputfilehandler_->max_index_per_ls_ &&
        outputfilehandler_->getCMSSWHeaders() &&
        outputfilehandler_->isMainPipeline()) {  // end of lumisection
      outputfilehandler_->write_EoLS_file(outputFile.Lumisection());
    }
  }
  LOG(TRACE) << "exiting close_and_rename. file_handler_running_ now " << file_handler_running_
             << " queue size now " << files_to_close_.size();
}

void OutputFileHandler::write_EoLS_file(uint32_t ls) {
  std::stringstream EoLS_filename_stringstream;
  EoLS_filename_stringstream << "run" << std::setfill('0') << std::setw(6) << current_run_number_
                             << "_ls" << std::setfill('0') << std::setw(4) << ls << "_EoLS.jsn";
  std::string EoLS_filename = EoLS_filename_stringstream.str();
  std::string full_filename = run_dir_ + "/" + EoLS_filename;
  std::stringstream EoLS_file_string_stream;
  std::fstream EoLS_file;
  LOG(TRACE) << "writing EoLS file " << EoLS_filename;
  EoLS_file.open(full_filename.c_str(), std::ios_base::out);

  // TODO: To me it looks like there are more fields than intended in this file...
  EoLS_file_string_stream << "{\n  \"data\":[\"" << ls_NOrbits_ << "\",\""  // NEvents
                          << ls_NFiles_ << "\",\""                          // NFiles
                          << ls_NOrbits_ << "\","                           // Total Events
                          << "\"0\",\""                                     // NLost Events
                          << ls_file_size_ << "\"],\n  \"definition\":\""   // NBytes
                          << "/fff/ramdisk/run" << std::setfill('0') << std::setw(6)
                          << current_run_number_ << "/jsd/EoLS.jsd\",\n  \"source\":\"l1scout\"\n}";

  ls_file_size_ = 0;
  run_NOrbits_ += ls_NOrbits_;
  run_NFiles_ += ls_NFiles_;
  ls_NOrbits_ = 0;
  ls_NFiles_ = 0;
  EoLS_file << EoLS_file_string_stream.str();
  EoLS_file.close();
}

void OutputFileHandler::write_EoR_file() {
  assert(current_index_ > 0 && nOrbitsPerFile_ > 0);
  int ls = int(1) + (current_index_ * nOrbitsPerFile_) / constants::N_orbits_per_lumisection;
  if (mainPipeline_) write_EoLS_file(ls);
  std::stringstream EoR_filename;
  EoR_filename << run_dir_ << "/"
               << "run" << std::setfill('0') << std::setw(6) << current_run_number_
               << "_ls0000_EoR.jsn";
  LOG(TRACE) << "Writing EoR file " << EoR_filename.str();
  std::fstream EoR_file;
  EoR_file.open(EoR_filename.str().c_str(), std::ios_base::out);
  std::stringstream EoR_file_string_stream;
  EoR_file_string_stream << "{\n  \"data\":[\"" << run_NOrbits_  // NEvents
                         << "\",\"" << run_NFiles_               // NFiles
                         << "\",\"" << ls << "\",\""             // NLumis
                         << ls << "\"],\n  \"definition\":\""    // LastLumi
                         << "/fff/ramdisk/run" << std::setfill('0') << std::setw(6)
                         << current_run_number_ << "/jsd/EoR.jsd\",\n  \"source\":\"l1scout\"\n}";
  EoR_file << EoR_file_string_stream.str();
  EoR_file.close();
}