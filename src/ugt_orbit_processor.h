#ifndef UGT_PROCESSOR_H
#define UGT_PROCESSOR_H

#include "orbit_processor.h"

class UgtOrbitProcessor : public OrbitProcessor {
  using OrbitProcessor::OrbitProcessor;

 protected:
  int ProcessBlock(MemRegion &readable_block, WriteMemRegion &writeable_block,
                   BxMetadata meta) override;

  inline uint32_t GetPacketSize() const override {
    // size of ugt bits packet is 4bytes*(2headerWords + 2frames*8dataWords)
    return 4 * (2 + (2 * 8));
  }

  inline uint32_t GetOrbitSizeInBytes(const FillOrbitMetadata &meta) const override {
    return GetPacketSize() * meta.filled_bxs;
  }
};

#endif  // UGT_PROCESSOR_H