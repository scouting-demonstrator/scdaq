#include "OutputByOrbit.h"

#include <cstdio>
#include <fstream>
#include <iostream>
#include <string>
#include <system_error>

#include "config.h"
#include "controls.h"
#include "log.h"
#include "tools.h"

#define EOR_DELAY_SECS 5

void OutputByOrbitStream::MonitorEndOfRun() {
  int latest_EoR = -1;
  while (true) {
    int current_run_number = output_file_handler_.GetCurrentRunNumber();
    if (!control_.running && latest_EoR != current_run_number) {
      // An unhandled run just stopped; wait N seconds in case the pipeline is still processing
      std::this_thread::sleep_for(std::chrono::seconds(EOR_DELAY_SECS));
      // Write EoR file for the most recent run
      if (output_file_handler_.isMainPipeline()) output_file_handler_.write_EoR_file();
      // Update local EoR tag
      latest_EoR = current_run_number;
    }
  }
}

OutputByOrbitStream::OutputByOrbitStream(ctrl &control, const ConfigView &conf_view)
    : tbb::filter(serial_in_order),
      totcounts_(0),
      control_(control),
      conf_view_(conf_view),
      conf_(conf_view_.conf_),
      output_file_handler_(
          conf_.output_file_base_, conf_.output_file_prefix_, conf_view_.GetOutputSuffix(),
          static_cast<int>(conf_.num_orbits_per_file_), conf_.support_cmssw_headers_,
          static_cast<int>(conf_view_.stream_conf_.source_id_)),
      min_buffer_queue_size_(1000) {
  LOG(TRACE) << "Created output filter at " << static_cast<void *>(this);

  monitoring_thread_ = std::thread(&OutputByOrbitStream::MonitorEndOfRun, this);
}

OutputByOrbitStream::~OutputByOrbitStream() {
  if (monitoring_thread_.joinable()) {
    monitoring_thread_.join();
  }
}

void OutputByOrbitStream::OutputFixedOrbits(Slice &out) {
  uint32_t orbitN = out.get_firstOrbitN();
  auto index = uint32_t(orbitN / conf_.num_orbits_per_file_);
  size_t n = 0;

  if ((out.get_counts() != 0) || conf_.support_cmssw_headers_) {
    if (control_.running.load(std::memory_order_acquire) ||
        control_.output_force_write) {  // i.e., should be writing data
      if (!out.isInitialized()) {
        return;
      }
      n = fwrite(out.begin(), 1, out.size(),
                 output_file_handler_.getFile(control_.run_number, index).getFilePtr());
      output_file_handler_.upFileSize(n);
      output_file_handler_.upNOrbits(conf_.num_orbits_per_packet_);
    } else if (output_file_handler_.hasFile()) {
      // the run has been stopped so drop but first check if there is a last
      // file to close
      LOG(TRACE) << "the run was stopped. queueing the last file for close and "
                    "rename ";
      output_file_handler_.enqueue_current_file_for_close_and_move_maybe();
      // TODO: Need to write EoR file here, probably.
    }
  }
}
void *OutputByOrbitStream::operator()(void *item) {
  Slice &out = *static_cast<Slice *>(item);
  totcounts_ += out.get_counts();
  OutputFixedOrbits(out);
  if (Slice::current_queue_size() < min_buffer_queue_size_) {
    LOG(TRACE) << "New minimum queue size is " << Slice::current_queue_size() << " total counts "
               << totcounts_;
    min_buffer_queue_size_ = Slice::current_queue_size();
  }
  Slice::giveAllocated(&out);
  return nullptr;
}
