#ifndef OUTPUTBYSIZE_H
#define OUTPUTBYSIZE_H

#include <tbb/pipeline.h>

#include <cstdint>
#include <cstdio>
#include <string>

#include "config.h"
#include "controls.h"

//! Filter that writes each buffer to a file.
class OutputBySizeStream : public tbb::filter {
 public:
  OutputBySizeStream(std::string output_filename_base, std::string output_filename_prefix,
                     std::string output_filename_suffix, ctrl &c,
                     Config::ProcessorType processorType_);
  void *operator()(void *item) override;

 private:
  void open_next_file();
  void close_and_move_current_file();

 private:
  const std::string output_filename_base_;
  const std::string output_filename_prefix_;
  const std::string output_filename_suffix_;
  uint32_t totcounts;
  uint64_t current_file_size;
  int32_t file_count;
  ctrl &control;
  FILE *current_file;
  uint32_t current_run_number;
  std::string journal_name;
  size_t min_buffer_queue_size_;
  bool bril_;
};

#endif
