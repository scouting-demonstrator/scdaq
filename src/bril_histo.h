#ifndef BRIL_HISTO_H
#define BRIL_HISTO_H

#include <tbb/pipeline.h>

#include <atomic>
#include <fstream>
#include <iostream>
#include <mutex>
#include <queue>
#include <utility>
#include <vector>

#include "controls.h"

template <typename T>
class BrilHistoQueue {
 public:
  void push(const T &value) {
    std::lock_guard<std::mutex> lock(m_mutex);
    m_queque.push(value);
  }

  void pop() {
    std::lock_guard<std::mutex> lock(m_mutex);
    m_queque.pop();
  }

 private:
  std::queue<T> m_queque;
  mutable std::mutex m_mutex;
};

#endif
