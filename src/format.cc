#include "format.h"

#include <cstdint>

bool CheckOrbitTrailerStart(const char* frame) {
  const auto word_array = reinterpret_cast<const uint32_t*>(frame);
  return word_array[0] == constants::beefdead;
}