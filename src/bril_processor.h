#ifndef BRIL_PROCESSOR_H
#define BRIL_PROCESSOR_H

#include "bril_histo.h"
#include "controls.h"
#include "format.h"
#include "processor.h"

class BrilProcessor : public Processor {
 public:
  explicit BrilProcessor(ctrl &control) : Processor(), control_(control) {}

  static BrilHistoQueue<
      std::array<uint32_t, constants::NBXPerOrbit + constants::NFramesInHistoHeader>>
      BrilQueue;

 protected:
  void ProcessSliceImpl(Slice &input, Slice &out) override;

 private:
  uint32_t FillBril(char *rd_ptr, char *wr_ptr, const char *end_ptr) const;

  ctrl &control_;
};

#endif  // BRIL_PROCESSOR_H