#include "bril_processor.h"

#include <cassert>

#include "bril_histo.h"
#include "format.h"

BrilHistoQueue<std::array<uint32_t, constants::NBXPerOrbit + constants::NFramesInHistoHeader>>
    BrilProcessor::BrilQueue;

void BrilProcessor::ProcessSliceImpl(Slice &input, Slice &out) {
  char *rd_ptr = input.begin();
  char *wr_ptr = out.begin();
  char *rd_end_ptr = input.end();

  uint32_t counts = FillBril(rd_ptr, wr_ptr, rd_end_ptr);
  out.set_end(out.begin() + counts);
  out.set_counts(counts);
  return;
}

uint32_t BrilProcessor::FillBril(char *rd_ptr, char *wr_ptr, const char *end_ptr) const {
  std::array<uint32_t, constants::NBXPerOrbit + constants::NFramesInHistoHeader> histo_arr;

  while (rd_ptr != end_ptr) {
    assert(rd_ptr + sizeof(brilFrame) - 1 <= end_ptr);

    auto *fr = reinterpret_cast<brilFrame *>(rd_ptr);

    if (fr->counter < constants::NFramesInHistoHeader) {
      if (fr->counter == 5) {  // add run number, 6th frame in bril header is run number
        histo_arr[fr->counter] = control_.run_number & 0xffffffff;
      } else {
        histo_arr[fr->counter] = (fr->word >> 0) & 0xffffffff;
      }

    } else {
      histo_arr[(fr->counter * 2) - constants::NFramesInHistoHeader] = (fr->word >> 0) & 0xffff;

      histo_arr[(fr->counter * 2) + 1 - constants::NFramesInHistoHeader] =
          (fr->word >> 16) & 0xffff;
    }
    rd_ptr += sizeof(brilFrame);
  }
  constexpr auto packed_size = static_cast<uint32_t>(sizeof(uint32_t)) *
                               (constants::NBXPerOrbit + constants::NFramesInHistoHeader);
  memcpy(wr_ptr, (char *)&histo_arr, packed_size);
  BrilQueue.push(histo_arr);

  return packed_size;
}