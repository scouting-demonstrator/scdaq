#ifndef SLICE_H
#define SLICE_H

#include <tbb/concurrent_queue.h>
#include <tbb/scalable_allocator.h>

#include <cstdint>

//! Holds a slice of data.
class Slice {
  //! Pointer to one past last filled byte in sequence
  char *logical_end;
  //! Pointer to one past last available byte in sequence.
  char *physical_end;
  uint32_t counts;
  bool output;
  static tbb::concurrent_bounded_queue<Slice *> free_slices;
  uint32_t firstOrbitN_;
  bool initialized_{false};

 public:
  //! Allocate a Slice object that can hold up to max_size bytes
  static Slice *allocate(size_t max_size) {
    // Replacing tbb::zero_allocator with aligned allocator.
    // Alignment to 32 bytes (256 bits) is required by MicronDMA.
    Slice *t = (Slice *)scalable_aligned_malloc(sizeof(Slice) + max_size, 32);

    t->logical_end = t->begin();
    t->physical_end = t->begin() + max_size;
    t->counts = 0;
    t->output = false;
    return t;
  }
  static Slice *preAllocate(size_t, size_t);
  static void shutDown();
  static Slice *getAllocated();
  static void giveAllocated(Slice *);
  static size_t current_queue_size();
  Slice *clone() const { return new Slice(*this); }

  void reset() {
    logical_end = begin();
    counts = 0;
    output = false;
    firstOrbitN_ = UINT_MAX;
    initialized_ = false;
  }

  //! Free a Slice object
  void free() { scalable_aligned_free(this); }
  //! Pointer to beginning of sequence
  char *begin() { return (char *)(this + sizeof(Slice)); }
  //! Pointer to one past last character in sequence
  char *end() { return logical_end; }
  //! Length of sequence
  size_t size() const { return logical_end - (char *)(this + sizeof(Slice)); }
  //! Maximum number of characters that can be appended to sequence
  size_t avail() const { return physical_end - logical_end; }
  //! Set end() to given value.
  void set_end(char *p) { logical_end = p; }
  void set_output(bool o) { output = o; }
  void set_counts(uint32_t c) { counts = c; }
  uint32_t get_counts() const { return counts; }
  uint32_t get_firstOrbitN() const { return firstOrbitN_; }
  void set_firstOrbitN(uint32_t firstOrbitN) {
    firstOrbitN_ = firstOrbitN;
    initialized_ = true;
  }
  bool isInitialized() { return initialized_; }
};
#endif
