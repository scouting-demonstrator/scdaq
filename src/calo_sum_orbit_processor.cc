#include "calo_sum_orbit_processor.h"

#include <cassert>

int CaloSumOrbitProcessor::ProcessBlock(MemRegion &readable_block, WriteMemRegion &writeable_block,
                                        BxMetadata meta) {
  struct BxMetadataType {
    uint32_t bx;
    int orbit;
  };

  using SourceDataType = blockCaloSum;

  assert(readable_block.CheckBounds(sizeof(SourceDataType)));

  SourceDataType *bl = readable_block.Scan<SourceDataType>();

  writeable_block.Fill(BxMetadataType{meta.bx, meta.orbit});
  writeable_block.Fill(bl->calo_sum, sizeof(bl->calo_sum));

  return 6;
}