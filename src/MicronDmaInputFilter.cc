#include "MicronDmaInputFilter.h"

#include <cctype>
#include <cstring>
#include <system_error>

#include "config.h"
#include "format.h"
#include "log.h"

MicronDmaInputFilter::MicronDmaInputFilter(size_t packetBufferSize, size_t nbPacketBuffers,
                                           ctrl &control, Config::ProcessorType processor_type)
    : InputFilter(packetBufferSize, nbPacketBuffers, control), processorType_(processor_type) {
  //   The RunBitFile function will locate a Pico card that can run the given bit
  //   file, and is not already opened in exclusive-access mode by another program.
  //   By default, it requests exclusive access to the Pico card so no other programs
  //   will try to reuse the card and interfere with us.
  // first argument sets share access to true for the pico driver,
  // to allow other programs to e.g., read registers
  LOG(TRACE) << "Finding pico driver...";
  int err = micron_find_pico_sb852(true, &pico_);

  if (err < 0) {
    // All functions in the Pico API return an error code.  If that code is < 0,
    // then you should use the PicoErrors_FullError function to decode the error message.
    auto error =
        "Find pico driver error " + std::string(micron_picoerrors_fullerror(stream2_, nullptr, 0));
    throw std::runtime_error(error);
  }
  LOG(TRACE) << "Opening streams to and from the FPGA";
  stream2_ = micron_create_stream(pico_, 2);

  if (stream2_ < 0) {
    auto error = "CreateStream error: " +
                 std::string(micron_picoerrors_fullerror(stream2_, nullptr, int(packetBufferSize)));
    throw std::runtime_error(error);
  }
  LOG(TRACE) << "Created Micron DMA input filter";
}

MicronDmaInputFilter::~MicronDmaInputFilter() {
  // streams are automatically closed when the PicoDrv object is destroyed, or
  // on program termination, but we can also close a stream manually.
  micron_close_stream(pico_, stream2_);
  LOG(TRACE) << "Destroyed Micron DMA input filter";
}

// add pad bytes to next multiple of 16 bytes
int pad_for_16bytes(int len) {
  int pad_len = len;
  if (len % 16 != 0) {
    pad_len = len + (16 - len % 16);
  }
  return pad_len;
}

ssize_t MicronDmaInputFilter::runMicronDAQ(char **buffer) {
  // Usually Pico streams come in two flavors: 4Byte wide, 16Byte wide
  // (equivalent to 32bit, 128bit respectively) However, all calls to ReadStream
  // and WriteStream must be 16Byte aligned (even for 4B wide streams) There is
  // also an 'undocumented' 32Byte wide (256 bit) stream.
  // We are using that stream here (and in the firmware).
  //
  // Now allocate 32B aligned space for read and write stream buffers
  //
  // Similarly, the size of the call, in bytes, must also be a multiple of 16Bytes.
  //
  // As with WriteStream, a ReadStream will block until all data is returned
  // from the FPGA to the host.
  //
  /////////////////////////////////
  // Note on reading streams///////
  // A user application will either have a deterministic amount of results, or a
  // non-deterministic amount.
  // When a non-deterministic amount of results are expected, and given the
  // blocking nature of the ReadStream,
  // a user should use the GetBytesAvailable() call to determine the amount of
  // data available for retrieval.
  // When a deterministic amount of results is expected, a user can skip the
  // performance impacting
  // GetBytesAvailable() call and request the full amount of results. The user
  // could also call ReadStream() iteratively, without GetBytesAvailable(), in
  // which case getting smaller chunks of results may allow additional
  // processing of the data on the host while the FPGA generates more results.
  // Either approach works, and should be kept in mind when tuning performance
  // of your application.
  ////////////////////////////////

  uint32_t packetSize;
  int err;

  if (processorType_ == Config::ProcessorType::BRIL) {
    packetSize = 32 * 1791;  // fixed bril 1-histo packet size
  } else {
    packetSize = 32 * (*((uint32_t *)((*buffer) + 8)) + 2);
  }

  // Here is where we actually call ReadStream
  // This reads "size" number of bytes of data from the output stream specified
  // by our stream handle (e.g. 'stream') into our host buffer (rbuf) This call
  // will block until it is able to read the entire "size" Bytes of data.
  err = micron_read_stream(pico_, stream2_, *buffer, static_cast<int>(packetSize));

  if (err < 0) {
    auto error = "ReadStream error: " + std::string(micron_picoerrors_fullerror(
                                            stream2_, *buffer, static_cast<int>(packetSize)));
    throw std::runtime_error(error);
  }
  return (packetSize);
}

// Read a packet from DMA
ssize_t MicronDmaInputFilter::readInput(char **buffer, size_t) { return runMicronDAQ(buffer); }

// Required for virtual function but currently unused
void MicronDmaInputFilter::print(std::ostream &) const {}
