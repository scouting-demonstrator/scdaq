#ifndef CONFIG_H
#define CONFIG_H

#include <json/json.h>

#include <boost/algorithm/string.hpp>
#include <cstdint>
#include <map>
#include <stdexcept>
#include <string>
#include <string_view>
#include <type_traits>

#include "log.h"

struct SconeAddress {
  std::string host;
  std::string port;
  std::string board;

  SconeAddress() = default;
  SconeAddress(std::string_view h, std::string_view p, std::string_view b)
      : host(h), port(p), board(b) {
    assert(!host.empty() && !port.empty() && !board.empty());
  }

  friend std::ostream &operator<<(std::ostream &os, const SconeAddress &scone) {
    os << scone.host << ":" << scone.port << "/" << scone.board;
    return os;
  }
};

class StreamConfig {
 public:
  const std::string processor_name_;
  const std::string primitive_name_;
  const uint32_t source_id_;
  const uint32_t tcp_dest_port_;

  StreamConfig(std::string processor, std::string primitive, uint32_t source_id, uint32_t tcp_port)
      : processor_name_(std::move(processor)),
        primitive_name_(std::move(primitive)),
        source_id_(source_id),
        tcp_dest_port_(tcp_port) {}

  friend std::ostream &operator<<(std::ostream &os, const StreamConfig &input) {
    os << "processor: " << input.processor_name_ << std::endl;
    os << "primitive: " << input.primitive_name_ << std::endl;
    os << "source_id: " << input.source_id_ << std::endl;
    os << "tcp_dest_port: " << input.tcp_dest_port_ << std::endl;
    return os;
  }
};

class ConfigMap {
 public:
  enum class EFormat { JSON5 };

  using ValueType = Json::Value;
  using ConstIteratorType = Json::Value::const_iterator;

  explicit ConfigMap(std::string_view);

  template <EFormat>
  int Parse(std::ifstream &input);

  // Insert (along with other functions) takes `key` as a const-ref since `ValueType`'s operator[]
  // does not support `std::string_view`.
  void Insert(const std::string &key, ValueType val) { data_[key] = std::move(val); }

  void Import(const ValueType &values) { data_ = values; }

  friend std::ostream &operator<<(std::ostream &os, const ConfigMap &map) {
    Json::StreamWriterBuilder builder;
    builder["indentation"] = " ";  // assume default for comments is None
    os << Json::writeString(builder, map.data_);
    return os;
  }

  template <typename T = std::string>
  T Get(const std::string &key, const ValueType &node) {
    const ValueType value = at(key, node);
    try {
      return ConvertValue<T>(value);
    } catch (std::exception &e) {
      LOG(ERROR) << "Key: " << key << "| Value '" << value.asString()
                 << "' | Conversion failed with exception: " << e.what();
      throw;
    }
  }

  template <typename T = std::string>
  T Get(const std::string &key) {
    return Get<T>(key, data_);
  }

  template <typename T>
  std::vector<T> GetArray(const std::string &key) {
    const ValueType array = at(key, data_);
    std::vector<T> output;
    for (int i = 0; i < static_cast<int>(array.size()); ++i) {
      try {
        output.emplace_back(ConvertValue<T>(array[i]));
      } catch (std::exception &e) {
        LOG(ERROR) << "Key: " << key << "| Value '" << array[i].asString()
                   << "' | Conversion failed with exception: " << e.what();
        throw;
      }
    }
    return output;
  }

  template <typename T>
  T ConvertValue(const ValueType &v) {
    if constexpr (std::is_same_v<T, StreamConfig>) {
      return StreamConfig(Get<std::string>("processor_type", v),
                          Get<std::string>("primitive_type", v), Get<uint32_t>("source_id", v),
                          Get<uint32_t>("tcp_dest_port", v));
    } else if constexpr (std::is_same_v<T, struct SconeAddress>) {
      return SconeAddress(Get<std::string>("host", v), Get<std::string>("port", v),
                          Get<std::string>("board", v));
    } else {
      std::string str(v.asString());
      if constexpr (std::is_same_v<T, std::string>)
        return str;
      else if constexpr (std::is_same_v<T, bool>) {
        // Special logic for booleans
        boost::algorithm::to_lower(str);
        return str == "true" || str == "yes";
      } else if constexpr (std::is_same_v<T, uint32_t>) {
        return static_cast<uint32_t>(std::stoul(str));
      } else if constexpr (std::is_same_v<T, uint64_t>) {
        return static_cast<uint64_t>(std::stoull(str));
      } else if constexpr (std::is_same_v<T, int>) {
        return static_cast<int>(std::stoi(str));
      } else {
        throw std::runtime_error("Failed to convert value '" + str + "'.");
      }
    }
  }

 private:
  ValueType data_;

  inline ValueType at(const std::string &key, const ValueType &node) const {
    try {
      const auto &val = node[key];
      return val;
    } catch (std::exception &e) {
      LOG(ERROR) << "Error fetching value for key '" << key << "'";
      LOG(ERROR) << e.what();
      throw;
    }
  }
};

class Config {
  ConfigMap dict_;

 public:
  enum class InputType { WZDMA, FILEDMA, TCPIP, MICRONDMA, FILE };
  enum class ProcessorType { PASS_THROUGH, GMT, CALO, CALOOBJ, CALOSUM, BMTF, UGT, BRIL };
  enum class PrimitiveType { NONE, MUON, JET, EGAMMA, TAU, SUM, STUB, ALGOBIT };

  explicit Config(std::string_view filename);
  void print() const;
  const StreamConfig &GetStreamConfigById(uint32_t stream_id) const;
  const std::string &GetInputFileById(uint32_t stream_id) const;

  ////// Setup configurations ////////////
  const InputType input_type_;
  const SconeAddress scone_address_;
  const std::string dma_device_;
  const uint32_t port_;
  const uint32_t num_threads_;

  // Log, Statistics
  bool attempt_tcp_reconnect_{false};
  uint32_t num_packets_per_report_;
  uint64_t num_packet_buffers_;
  uint64_t packet_buffer_size_;
  LOG_LEVEL min_log_severity_{LOG_LEVEL::ERROR};

  ////// Run configurations ////////////
  // Input
  const std::string input_file_;
  const uint32_t num_orbits_per_packet_;
  std::vector<std::string> input_files_;
  std::vector<struct StreamConfig> input_streams_;
  // Processor
  const bool enable_processing_;
  const bool apply_zero_suppression_;
  // Output
  const std::string output_file_base_;
  const std::string output_file_prefix_;
  const std::string output_file_suffix_;
  bool force_write_out_{false};

  // File Stream outputs
  const uint64_t max_file_size_;

  // Orbit Stream outputs
  const bool support_cmssw_headers_;
  const bool support_dth_headers_;
  const int64_t num_orbits_per_file_;

  friend std::ostream &operator<<(std::ostream &os, const Config &config) {
    os << config.dict_ << std::endl;
    os << "Number of input streams / output files: " << config.input_streams_.size() << std::endl;
    os << "Number of orbits per file: " << config.num_orbits_per_file_ << std::endl;
    os << "Max file size: " << config.max_file_size_ << std::endl;
    os << "Force write-out? " << (config.force_write_out_ ? "true" : "false") << std::endl;
    return os;
  }

  inline static LOG_LEVEL ToLOG_LEVEL(const std::string &in) {
    std::string str = boost::algorithm::to_upper_copy(in);
    if (str == "TRACE") return TRACE;
    if (str == "DEBUG") return DEBUG;
    if (str == "INFO") return INFO;
    if (str == "WARNING") return WARNING;
    if (str == "ERROR") return ERROR;
    if (str == "FATAL") return FATAL;
    throw std::invalid_argument("Configuration error: Unknown log level '" + str + "'");
  }

  inline static InputType ToInputType(const std::string &in) {
    std::string str = boost::algorithm::to_upper_copy(in);
    if (str == "WZDMA") return InputType::WZDMA;
    if (str == "FILEDMA") return InputType::FILEDMA;
    if (str == "TCPIP") return InputType::TCPIP;
    if (str == "MICRONDMA") return InputType::MICRONDMA;
    if (str == "FILE") return InputType::FILE;
    throw std::invalid_argument("Configuration error: Wrong input type '" + str + "'");
  }

  inline static ProcessorType ToProcessorType(const std::string &in) {
    std::string str = boost::algorithm::to_upper_copy(in);
    if (str == "PASS_THROUGH") return ProcessorType::PASS_THROUGH;
    if (str == "GMT") return ProcessorType::GMT;
    if (str == "CALO") return ProcessorType::CALO;
    if (str == "CALOOBJ") return ProcessorType::CALOOBJ;
    if (str == "CALOSUM") return ProcessorType::CALOSUM;
    if (str == "BMTF") return ProcessorType::BMTF;
    if (str == "UGT") return ProcessorType::UGT;
    if (str == "BRIL") return ProcessorType::BRIL;
    throw std::invalid_argument("Configuration error: Wrong processor type '" + str + "'");
  }

  inline static PrimitiveType ToPrimitiveType(const std::string &in) {
    std::string str = boost::algorithm::to_upper_copy(in);
    if (str == "NONE") return PrimitiveType::NONE;
    if (str == "MUON") return PrimitiveType::MUON;
    if (str == "JET") return PrimitiveType::JET;
    if (str == "EGAMMA") return PrimitiveType::EGAMMA;
    if (str == "TAU") return PrimitiveType::TAU;
    if (str == "SUM") return PrimitiveType::SUM;
    if (str == "STUB") return PrimitiveType::STUB;
    throw std::invalid_argument("Configuration error: Wrong primitive type '" + str + "'");
  }
};

// TODO(@glazzari): Provisionally gimped implementation of `ConfigView`
class ConfigView {
 public:
  const Config &conf_;
  const StreamConfig stream_conf_;
  const uint32_t stream_id_;

  ConfigView(const Config &c, uint32_t i)
      : conf_(c), stream_conf_(c.GetStreamConfigById(i)), stream_id_(i) {
    LOG(TRACE) << "Created ConfigView with id=" << stream_id_;
  }

  const std::string &GetInputFile() const { return conf_.GetInputFileById(stream_id_); }

  Config::ProcessorType GetProcessorType() const {
    return Config::ToProcessorType(stream_conf_.processor_name_);
  }

  Config::PrimitiveType GetPrimitiveType() const {
    return Config::ToPrimitiveType(stream_conf_.primitive_name_);
  }

  std::string GetOutputSuffix() const {
    // The numeric appendix indicating the stream is omitted for the first output file.
    // E.g., for 3 streams, the expected naming convention is approximately as follows:
    // ["basename_orbitNumber.raw", "basename_orbitNumber.raw_1", "basename_orbitNumber.raw_2"]
    if (stream_id_ == 0)
      return conf_.output_file_suffix_;
    else
      return conf_.output_file_suffix_ + "_" + std::to_string(stream_id_);
  }
};

#endif
