#ifndef CALO_OBJ_PROCESSOR_H
#define CALO_OBJ_PROCESSOR_H

#include "config.h"
#include "format.h"
#include "orbit_processor.h"

class CaloObjOrbitProcessor : public OrbitProcessor {
 public:
  CaloObjOrbitProcessor(uint32_t num_orbits_per_packet, bool apply_zs, bool cmssw_headers,
                        bool dth_headers, Config::PrimitiveType primitive_type, uint16_t source_id,
                        ctrl &control)
      : OrbitProcessor(num_orbits_per_packet, apply_zs, cmssw_headers, dth_headers, primitive_type,
                       source_id, control) {
    switch (primitive_type) {
      case Config::PrimitiveType::JET:
        shiftsCaloObjEt = shiftsCaloJet::ET;
        maskCaloObjEt = masksCaloJet::ET;
        break;
      case Config::PrimitiveType::EGAMMA:
        shiftsCaloObjEt = shiftsCaloEGamma::ET;
        maskCaloObjEt = masksCaloEGamma::ET;
        break;
      case Config::PrimitiveType::TAU:
        shiftsCaloObjEt = shiftsCaloTau::ET;
        maskCaloObjEt = masksCaloTau::ET;
        break;
      default:
        throw std::invalid_argument("Error: Unrecognized primitive type.");
    }
  }

 protected:
  int ProcessBlock(MemRegion &readable_block, WriteMemRegion &writeable_block,
                   BxMetadata meta) override;

  inline uint32_t GetPacketSize() const override {
    // maximum size of calo object packet is 4bytes*(3headerWords + 12objects)=60
    return 4 * (3 + 12);
  }

  inline uint32_t GetOrbitSizeInBytes(const FillOrbitMetadata &meta) const override {
    return meta.counts * 4 + 12 * meta.filled_bxs;
  }

  uint32_t shiftsCaloObjEt;
  uint32_t maskCaloObjEt;
};

#endif  // CALO_OBJ_PROCESSOR_H
