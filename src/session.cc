#include "session.h"

#include <string>

const std::string session::reply_success = "ok   ";
const std::string session::reply_failure = "error";
