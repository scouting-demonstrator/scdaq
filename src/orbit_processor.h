#ifndef ORBIT_PROCESSOR_H
#define ORBIT_PROCESSOR_H

#include <cassert>

#include "config.h"
#include "controls.h"
#include "format.h"
#include "processor.h"

class OrbitProcessor : public Processor {
 public:
  OrbitProcessor(uint32_t num_orbits_per_packet, bool apply_zs, bool cmssw_headers,
                 bool dth_headers, Config::PrimitiveType primitive_type, uint16_t source_id,
                 ctrl &control)
      : nOrbitsPerPacket(num_orbits_per_packet),
        doZS(apply_zs),
        cmsswHeaders(cmssw_headers),
        dthHeaders(dth_headers),
        source_id_(source_id),
        control_(control) {
    switch (primitive_type) {
      case Config::PrimitiveType::MUON:
        bsize = sizeof(blockMuon);
        break;
      case Config::PrimitiveType::JET:
      case Config::PrimitiveType::EGAMMA:
      case Config::PrimitiveType::TAU:
        bsize = sizeof(blockCaloObj);
        break;
      case Config::PrimitiveType::SUM:
        bsize = sizeof(blockCaloSum);
        break;
      case Config::PrimitiveType::ALGOBIT:
        bsize = sizeof(blockUgt);
        break;
      case Config::PrimitiveType::STUB:
        bsize = sizeof(blockBmtf);
        break;
      case Config::PrimitiveType::NONE:
        bsize = 0;
        break;
      default:
        throw std::invalid_argument("Error: Unrecognized primitive type");
    }
  }

 protected:
  struct FillOrbitMetadata {
    uint32_t counts;
    int orbit;
    uint32_t filled_bxs;
  };

  template <typename T, uint32_t N>
  class DataBuffer {
   public:
    DataBuffer(T *input_data, size_t length)
        : data(input_data, input_data + (length * sizeof(T))), count(length) {}
    DataBuffer() = default;

    uint32_t GetSize() const { return count * sizeof(T); }

    T *GetBuffer() { return data; }

    void Add(T v) {
      assert(count < N);
      data[count++] = v;
    }

    const static uint32_t max_elem = N;
    T data[max_elem];
    uint32_t count = 0;
  };

  struct BxMetadata {
    uint32_t header;
    uint32_t bx;
    int orbit;
  };

  template <typename T, uint32_t N>
  using BxData = DataBuffer<T, N>;

  // Pointer manager. Make sure it's aligned
  class MemRegion {
   public:
    MemRegion(char **ptr, const char *end) : ptr_(ptr), ptr_end_(end) {}

    bool CheckBounds(size_t size) { return (GetAvailable() - static_cast<int64_t>(size)) >= 0; }

    void Increment(size_t size) { (*ptr_) += size; }

    char **GetDoublePtr() { return ptr_; }

    template <typename T>
    T *Scan() {
      T *view = reinterpret_cast<T *>(*ptr_);
      Increment(sizeof(T));
      return view;
    }

    int64_t GetAvailable() { return ptr_end_ - *ptr_; }

   protected:
    char **ptr_;
    const char *ptr_end_;
  };

  class WriteMemRegion : public MemRegion {
    using MemRegion::MemRegion;

   public:
    template <typename T>
    void Fill(T data) {
      assert(CheckBounds(sizeof(T)));
      *reinterpret_cast<T *>(*ptr_) = data;
      Increment(sizeof(T));
    }

    template <typename T>
    void Fill(T *data, uint32_t size) {
      assert(CheckBounds(size));
      memcpy(*ptr_, (char *)data, size);
      Increment(size);
    }
  };

  bool HasTrailer(Slice &input, char *&rd_ptr) const;
  bool CheckFrameMultBlock(size_t inputSize, uint16_t nDroppedOrbitsInPacket) const;
  std::pair<uint32_t, bool> ProcessOrbitHeader(char *rd_ptr);
  size_t fillFRDEventHeader_V6(char *wr_ptr_FRDHead, uint32_t inputSize, uint32_t orbit) const;

  FillOrbitMetadata FillOrbit(orbit_trailer *trailer, char *rd_ptr, char *wr_ptr,
                              const char *rd_end_ptr, const char *wr_end_ptr,
                              bool is_dropped_orbit);

  void ProcessSliceImpl(Slice &input, Slice &out) override;

  virtual int ProcessBlock(MemRegion &readable_block, WriteMemRegion &writeable_block,
                           BxMetadata meta) = 0;

  virtual uint32_t GetPacketSize() const = 0;

  virtual uint32_t GetOrbitSizeInBytes(const FillOrbitMetadata &meta) const = 0;

  const uint32_t nOrbitsPerPacket;
  const bool doZS;
  int bsize;
  const bool cmsswHeaders;
  const bool dthHeaders;
  const uint16_t source_id_;

  ctrl &control_;
};

#endif  // ORBIT_PROCESSOR_H
