#ifndef FORMAT_H
#define FORMAT_H

#include <cmath>
#include <cstdint>

struct packet_header {
  uint64_t feedbeef;
  uint32_t source_id;
  uint32_t frames_in_packet;
  uint32_t orbit_dropped_counter;
  uint32_t orbit_seen_counter;
  uint16_t orbit_in_packet;
  uint16_t dropped_orbit_in_packet;
  uint32_t empty;
};

struct fragment_header {
  uint16_t flag_fh;
  uint16_t flag_dropped_orbit;
  uint32_t orbit_length_frames;
  uint16_t orbit_length_bxs;
  uint16_t empty;
  uint32_t orbit_number;
  uint64_t dropped_orbits_counter;
  uint64_t seen_orbits_counter;
};

struct blockCalo {
  uint32_t calo0[8];
  uint32_t calo1[8];
  uint32_t calo2[8];
  uint32_t calo3[8];
  uint32_t calo4[8];
  uint32_t calo5[8];
};

struct blockCaloObj {
  uint32_t calo_obj[12];
  uint32_t empty[4];
};

struct blockCaloSum {
  uint32_t calo_sum[6];
  uint32_t empty[2];
};

struct block_calo_packed {
  uint32_t header;
  uint32_t bx;
  uint32_t orbit;
  uint32_t frame[56];  //+8 for extra word containing link number;
};

struct blockMuon {
  uint32_t orbit[4];
  uint32_t bx[4];
  uint32_t mu1f[4];
  uint32_t mu1s[4];
  uint32_t mu2f[4];
  uint32_t mu2s[4];
};

struct blockBmtf {
  uint64_t stub[8];
};

struct blockUgt {
  uint32_t ugt_32bit[16];
};

struct brilFrame {
  uint32_t word;
  uint32_t counter;
  uint32_t emptyLinks[6];
};

struct bx_map_frame {
  uint32_t bx_map_l[8];
};

struct orbit_trailer {
  uint32_t beefdead[8];
  uint32_t bx_map[14 * 8];
  uint32_t trailer1[8];
};

struct shiftsCaloTau {
  static constexpr uint32_t ET = 0;
  static constexpr uint32_t eta = 9;
  static constexpr uint32_t phi = 17;
  static constexpr uint32_t iso = 25;
};

struct masksCaloJet {
  static constexpr uint32_t ET = 0x07ff;
  static constexpr uint32_t eta = 0x00ff;
  static constexpr uint32_t phi = 0x00ff;
};
struct shiftsCaloJet {
  static constexpr uint32_t ET = 0;
  static constexpr uint32_t eta = 11;
  static constexpr uint32_t phi = 19;
};
struct masksCaloEGamma {
  static constexpr uint32_t ET = 0x01ff;
  static constexpr uint32_t eta = 0x00ff;
  static constexpr uint32_t phi = 0x00ff;
  static constexpr uint32_t iso = 0x0003;
};

struct shiftsCaloEGamma {
  static constexpr uint32_t ET = 0;
  static constexpr uint32_t eta = 9;
  static constexpr uint32_t phi = 17;
  static constexpr uint32_t iso = 25;
};

struct masksCaloTau {
  static constexpr uint32_t ET = 0x01ff;
  static constexpr uint32_t eta = 0x00ff;
  static constexpr uint32_t phi = 0x00ff;
  static constexpr uint32_t iso = 0x0003;
};

struct masksCaloESums {
  static constexpr uint32_t ETEt = 0x0fff;  // Et of ET object
  static constexpr uint32_t ETEttem = 0x0fff;
  static constexpr uint32_t ETMinBiasHF = 0x000f;

  static constexpr uint32_t HTEt = 0x0fff;  // ET of HT object
  static constexpr uint32_t HTtowerCount = 0x1fff;
  static constexpr uint32_t HTMinBiasHF = 0x0003;

  static constexpr uint32_t ETmissEt = 0x0fff;
  static constexpr uint32_t ETmissPhi = 0x00ff;
  static constexpr uint32_t ETmissASYMET = 0x00ff;
  static constexpr uint32_t ETmissMinBiasHF = 0x0003;

  static constexpr uint32_t HTmissEt = 0x0fff;
  static constexpr uint32_t HTmissPhi = 0x00ff;
  static constexpr uint32_t HTmissASYMHT = 0x00ff;
  static constexpr uint32_t HTmissMinBiasHF = 0x0003;

  static constexpr uint32_t ETHFmissEt = 0x0fff;
  static constexpr uint32_t ETHFmissPhi = 0x00ff;
  static constexpr uint32_t ETHFmissASYMETHF = 0x00ff;
  static constexpr uint32_t ETHFmissCENT = 0x0003;

  static constexpr uint32_t HTHFmissEt = 0x0fff;
  static constexpr uint32_t HTHFmissPhi = 0x00ff;
  static constexpr uint32_t HTHFmissASYMHTHF = 0x00ff;
  static constexpr uint32_t HTHFmissCENT = 0x0003;
};

struct shiftsCaloESums {
  static constexpr uint32_t ETEt = 0;  // Et of ET object
  static constexpr uint32_t ETEttem = 12;
  static constexpr uint32_t ETMinBiasHF = 28;

  static constexpr uint32_t HTEt = 0;  // ET of HT object
  static constexpr uint32_t HTtowerCount = 12;
  static constexpr uint32_t HTMinBiasHF = 28;

  static constexpr uint32_t ETmissEt = 0;
  static constexpr uint32_t ETmissPhi = 12;
  static constexpr uint32_t ETmissASYMET = 20;
  static constexpr uint32_t ETmissMinBiasHF = 28;

  static constexpr uint32_t HTmissEt = 0;
  static constexpr uint32_t HTmissPhi = 12;
  static constexpr uint32_t HTmissASYMHT = 20;
  static constexpr uint32_t HTmissMinBiasHF = 28;

  static constexpr uint32_t ETHFmissEt = 0;
  static constexpr uint32_t ETHFmissPhi = 12;
  static constexpr uint32_t ETHFmissASYMETHF = 20;
  static constexpr uint32_t ETHFmissCENT = 28;

  static constexpr uint32_t HTHFmissEt = 0;
  static constexpr uint32_t HTHFmissPhi = 12;
  static constexpr uint32_t HTHFmissASYMHTHF = 20;
  static constexpr uint32_t HTHFmissCENT = 28;
};

struct shiftsBmtfStub {
  static constexpr uint32_t valid = 0;
  static constexpr uint32_t phi = 1;
  static constexpr uint32_t phiB = 13;
  static constexpr uint32_t qual = 23;
  static constexpr uint32_t eta = 26;
  static constexpr uint32_t qeta = 33;
  static constexpr uint32_t station = 40;
  static constexpr uint32_t wheel = 42;
  static constexpr uint32_t reserved = 45;
};

struct masksBmtfStub {
  static constexpr uint32_t valid = 0x0001;
  static constexpr uint32_t phi = 0x0fff;
  static constexpr uint32_t phiB = 0x03ff;
  static constexpr uint32_t qual = 0x0007;
  static constexpr uint32_t eta = 0x007f;
  static constexpr uint32_t qeta = 0x007f;
  static constexpr uint32_t station = 0x0003;
  static constexpr uint32_t wheel = 0x0007;
  static constexpr uint32_t reserved = 0x0007;
};

struct muon {
  uint32_t f;      // first word
  uint32_t s;      // second word
  uint32_t extra;  // for intermediate marker and eta
};

struct block {
  uint32_t bx;
  uint32_t orbit;
  muon mu[16];
};

// run3 format --tj
struct masks {
  // bx word: 16 bits used for actual bx, MS 4 bits are muon type
  //(0xf intermediate, 0x0 final, following 4 bits for link id)
  static constexpr uint32_t bx = 0x1fff;
  static constexpr uint32_t interm = 0x0001;
  // masks for muon 64 bits
  static constexpr uint32_t phiext = 0x03ff;
  static constexpr uint32_t pt = 0x01ff;
  static constexpr uint32_t ptuncon = 0x00ff;  //--8bits
  static constexpr uint32_t qual = 0x000f;
  static constexpr uint32_t etaext = 0x01ff;
  static constexpr uint32_t etaextv = 0x00ff;
  static constexpr uint32_t etaexts = 0x0100;
  static constexpr uint32_t iso = 0x0003;
  static constexpr uint32_t chrg = 0x0001;
  static constexpr uint32_t chrgv = 0x0001;
  static constexpr uint32_t index = 0x007f;
  static constexpr uint32_t phi = 0x03ff;
  static constexpr uint32_t eta = 0x01ff;
  static constexpr uint32_t etav = 0x00ff;
  static constexpr uint32_t etas = 0x0100;
  static constexpr uint32_t impact = 0x0003;
};

struct shifts {
  // bx word: 16 bits used for actual bx, MS 4 bits are muon type
  //(0xf intermediate, 0x0 final, following 4 bits for link id)
  static constexpr uint32_t bx = 0;
  static constexpr uint32_t interm = 31;  // updated for new run3 format //tj
  // shifts for muon 64 bits
  static constexpr uint32_t phiext = 0;
  static constexpr uint32_t pt = 10;
  static constexpr uint32_t qual = 19;
  static constexpr uint32_t etaext = 23;
  static constexpr uint32_t iso = 0;
  static constexpr uint32_t chrg = 2;
  static constexpr uint32_t chrgv = 3;
  static constexpr uint32_t index = 4;
  static constexpr uint32_t phi = 11;
  static constexpr uint32_t eta1 = 13;
  static constexpr uint32_t eta2 = 22;
  static constexpr uint32_t ptuncon = 21;
  static constexpr uint32_t impact = 30;
};

struct packed_format_header_shifts {
  static constexpr uint32_t bxmatch = 24;
  static constexpr uint32_t mAcount = 16;
  static constexpr uint32_t orbitmatch = 8;
  static constexpr uint32_t warningTestEnabled = 8;
  static constexpr uint32_t mBcount = 0;
};

struct packed_format_header_masks {
  static constexpr uint32_t bxmatch = 0x00ff << packed_format_header_shifts::bxmatch;
  static constexpr uint32_t mAcount = 0x000f << packed_format_header_shifts::mAcount;
  static constexpr uint32_t warningTestEnabled = 0x0001
                                                 << packed_format_header_shifts::warningTestEnabled;
  static constexpr uint32_t orbitmatch = 0x00ff << packed_format_header_shifts::orbitmatch;
  static constexpr uint32_t mBcount = 0x000f << packed_format_header_shifts::mBcount;
};

struct gmt_scales {
  static constexpr float pt_scale = 0.5f;
  static constexpr float ptunconstrained_scale = 1.0f;
  static constexpr float phi_scale = static_cast<float>(2. * M_PI / 576.);
  static constexpr float eta_scale = static_cast<float>(0.0870 / 8.);  // 9th MS bit is sign
  static constexpr float phi_range = M_PI;
};

struct calo_scales {
  static constexpr float phi_scale = static_cast<float>(2. * M_PI / 144.);
  static constexpr float eta_scale = 0.0435f;
  static constexpr float et_scale = 0.5f;
};

struct constants {
  static constexpr uint32_t deadbeef = 0xdeadbeef;
  static constexpr uint32_t beefdead = 0xbeefdead;
  static constexpr uint32_t intermediate_marker = 0x0000000f;
  static constexpr uint32_t orbit_trailer_size = 512;
  static constexpr uint32_t orbit_header_size = 32;
  static constexpr uint32_t dma_trailer_size = 32;
  // Note: total number of bytes of overhead per packet is:
  // (orbit_trailer_size+orbit_header_size)*nOrbitsPerPacket +
  // dma_trailer_size
  static constexpr uint32_t intermediate = 0x00000001;
  static constexpr uint32_t final = 0x00000001;
  static constexpr uint32_t bril_header = 4278781695;
  static constexpr uint32_t NBXPerOrbit = 3564;
  static constexpr uint32_t NFramesInHistoHeader = 9;
  static constexpr uint64_t N_orbits_per_lumisection = 1 << 18;  // equivalent to pow(2, 18)
};

bool CheckOrbitTrailerStart(const char* frame);

#endif
