#include "pipeline.h"

#include "bmtf_orbit_processor.h"
#include "bril_processor.h"
#include "calo_obj_orbit_processor.h"
#include "calo_orbit_processor.h"
#include "calo_sum_orbit_processor.h"
#include "muon_orbit_processor.h"
#include "orbit_processor.h"
#include "processor.h"
#include "ugt_orbit_processor.h"

template <typename T>
std::shared_ptr<OrbitProcessor> MakeA(const ConfigView &conf_view, ctrl &control) {
  auto &conf = conf_view.conf_;
  return std::make_shared<T>(conf.num_orbits_per_packet_, conf.enable_processing_,
                             conf.support_cmssw_headers_, conf.support_dth_headers_,
                             conf_view.GetPrimitiveType(),
                             static_cast<uint16_t>(conf_view.stream_conf_.source_id_), control);
}

std::shared_ptr<Processor> MakeProcessor(const ConfigView &conf_view, ctrl &control) {
  switch (conf_view.GetProcessorType()) {
    case Config::ProcessorType::GMT:
      return MakeA<MuonOrbitProcessor>(conf_view, control);
    case Config::ProcessorType::CALO:
      return MakeA<CaloOrbitProcessor>(conf_view, control);
    case Config::ProcessorType::CALOOBJ:
      return MakeA<CaloObjOrbitProcessor>(conf_view, control);
    case Config::ProcessorType::CALOSUM:
      return MakeA<CaloSumOrbitProcessor>(conf_view, control);
    case Config::ProcessorType::BMTF:
      return MakeA<BmtfOrbitProcessor>(conf_view, control);
    case Config::ProcessorType::UGT:
      return MakeA<UgtOrbitProcessor>(conf_view, control);
    case Config::ProcessorType::BRIL:
      return std::make_shared<BrilProcessor>(control);
    case Config::ProcessorType::PASS_THROUGH:
      return std::make_shared<Processor>();
    default:
      throw std::invalid_argument("Error: Unrecognized processor type.");
  }
}

Pipeline::Pipeline(ConfigView conf_view, uint max_tokens_per_thread)
    : conf_view_(std::move(conf_view)), max_tokens_per_thread_(max_tokens_per_thread) {
  const Config &conf = conf_view_.conf_;
  const auto &stream_conf = conf_view_.stream_conf_;
  tools::log::set_filter_min_severity(conf.min_log_severity_);

  LOG(INFO) << "Loaded configuration with following setup: \n" << conf;

  const Config::InputType &input = conf.input_type_;
  const size_t &num_packet_buffers = conf.num_packet_buffers_;
  const size_t &packet_buffer_size = conf.packet_buffer_size_;

  if (input == Config::InputType::FILEDMA) {
    input_filter_ = std::make_shared<FileDmaInputFilter>(
        conf_view_.GetInputFile(), packet_buffer_size, num_packet_buffers, control_);

  } else if (input == Config::InputType::WZDMA) {
    input_filter_ = std::make_shared<WZDmaInputFilter>(packet_buffer_size, num_packet_buffers,
                                                       control_, conf.scone_address_);

  } else if (input == Config::InputType::MICRONDMA) {
    input_filter_ = std::make_shared<MicronDmaInputFilter>(packet_buffer_size, num_packet_buffers,
                                                           control_, conf_view_.GetProcessorType());

  } else if (input == Config::InputType::TCPIP) {
    if (conf.attempt_tcp_reconnect_) {
      input_filter_ = std::make_shared<TcpInputFilter>(stream_conf.tcp_dest_port_,
                                                       packet_buffer_size, num_packet_buffers,
                                                       conf.num_orbits_per_packet_, control_);
    } else {
      throw std::invalid_argument(
          "Configuration error: enable developmentMode to use TCP input filter");
    }
  } else {
    throw std::invalid_argument("Configuration error: Unknown input type was specified");
  }

  // Add input reader to a pipeline
  pipeline_.add_filter(*input_filter_);

  // Create reformatter and add it to the pipeline
  if (conf.enable_processing_) {
    stream_processor_ = MakeProcessor(conf_view_, control_);
    pipeline_.add_filter(*stream_processor_);
  }

  // Create file-writing stage and add it to the pipeline
  if (conf.num_orbits_per_file_) {
    output_stream_ = std::make_shared<OutputByOrbitStream>(control_, conf_view_);
  } else {
    output_stream_ = std::make_shared<OutputBySizeStream>(
        conf.output_file_base_, conf.output_file_prefix_, conf_view_.GetOutputSuffix(), control_,
        conf_view_.GetProcessorType());
  }
  pipeline_.add_filter(*output_stream_);

  control_.running = false;
  control_.run_number = 0;
  control_.max_file_size = conf.max_file_size_;  // in bytes
  control_.packets_per_report = static_cast<int>(conf.num_packets_per_report_);
  control_.output_force_write = conf.force_write_out_;
  control_.n_orbits_per_packet = static_cast<int>(conf.num_orbits_per_packet_);

  // Firmware needs at least 1MB buffer for DMA
  if (packet_buffer_size < 1024 * 1024) {
    LOG(ERROR) << "dma_packet_buffer_size must be at least 1048576 bytes (1MB), but "
               << packet_buffer_size << " bytes was given. Check the configuration file.";
    throw std::invalid_argument("Configuration error: DMA packet buffer size too small. ");
  }
}

MultiPipeline::MultiPipeline(Config c) : conf_(std::move(c)) {
  LOG(DEBUG) << "Loaded configuration:";
  LOG(DEBUG) << conf_;
  uint32_t num_pipelines = conf_.input_streams_.size();
  for (uint32_t pipeline_id = 0; pipeline_id < num_pipelines; pipeline_id++) {
    try {
      pipelines_.emplace_back(ConfigView{conf_, pipeline_id});
      LOG(DEBUG) << "-Pipeline set up";
    } catch (std::invalid_argument &e) {
      LOG(FATAL) << "-Configuration invalid! Error text is \"" << e.what() << "\" Bailing out.";
      throw;
    } catch (std::exception &e) {
      LOG(ERROR) << "-Error occurred. error text is: \"" << e.what() << "\"";
      throw;
    }
  }

  {  // RAII
    server_ = std::unique_ptr<server>(new server(io_service_, conf_.port_, Pipeline::control_));
    server_thread_ = boost::thread(boost::bind(&boost::asio::io_service::run, &io_service_));
  }
}

int MultiPipeline::Run() {
  int retval = 0;
  assert(!pipelines_.empty());
  try {
    if (!RunPipelines(static_cast<unsigned int>(pipelines_.size()))) {
      retval = 1;  // Will terminate with error code set
    }
  } catch (std::exception &e) {
    LOG(ERROR) << "Error in pipelines. Error text is: \"" << e.what() << "\"";
    retval = 1;
    // Use info from first file for SCONE reset
    LOG(INFO) << "Resetting board at http://" << conf_.scone_address_ << " before exiting.. ";
    int res = tools::reset_board(conf_.scone_address_.host, conf_.scone_address_.port,
                                 conf_.scone_address_.board);
    if (res != 0) {
      LOG(ERROR) << "Reset failed! Error code: " << res;
    } else {
      LOG(INFO) << "Successfully reset the board.";
    }
  }

  LOG(DEBUG) << "Terminating the internal TCP server.";
  io_service_.stop();
  if (!server_thread_.try_join_for(boost::chrono::milliseconds(2000))) {
    LOG(ERROR) << "Failed to terminate the internal TCP server, the program "
                  "may crash on exit.";
  } else {
    LOG(DEBUG) << "Internal TCP server is terminated.";
  }

  return retval;
}

int MultiPipeline::RunPipelines(uint num_streams) {
  tbb::task_group group;
  // Run pipelines and measure real time
  tbb::tick_count t0 = tbb::tick_count::now();
  for (uint i = 0; i < num_streams; ++i) {
    auto &p = this->pipelines_.at(i);
    group.run([&] {
      return p.pipeline_.run(p.conf_view_.conf_.num_threads_ * p.max_tokens_per_thread_);
    });
  }
  group.wait();
  tbb::tick_count t1 = tbb::tick_count::now();

  if (verbose_) {
    LOG(INFO) << "time = " << (t1 - t0).seconds();
  }
  return 1;
}
