#pragma once

#include <cctype>
#include <iomanip>
#include <iostream>
#include <mutex>
#include <sstream>
#include <thread>

/*
 * Simple logging macros, replace later with boost when we have a version with
 * logging...
 */

// #include <boost/log/trivial.hpp>

// Uncomment to include a thread ID in the log message
// #define LOG_THREAD_ID

enum LOG_LEVEL { TRACE, DEBUG, INFO, WARNING, ERROR, FATAL };

// Make a better function name but not so complicated
inline std::string simplify_pretty(const char *fname) {
  std::string::size_type pos = 0;
  std::string sfname(fname);

  // Get the function name, i.e. '...foo(' without '('
  while (true) {
    pos = sfname.find('(', pos);
    if (pos == std::string::npos) {
      break;
    } else {
      if (pos > 0) {
        // Check if the previous character is graphics (it may be the end of the
        // function)
        if (isgraph(sfname[pos - 1])) {
          // We have the function name
          break;
        }
      }
      pos++;
    }
  }

  if (pos == std::string::npos) {
    // Failed to obtain the name, return the full one
    return sfname;
  }

  // Get the function name
  std::string lfname = sfname.substr(0, pos);

  // Clean the function name
  pos = lfname.find_last_of(' ');

  if (pos != std::string::npos) {
    lfname = lfname.substr(pos + 1);
  }

  return lfname;
}

#ifdef LOG_THREAD_ID
#define LOG(severity)                                                                      \
  if (severity >= tools::log::severity_filter)                                             \
  tools::log::log() << "[0x" << std::hex << std::this_thread::get_id() << std::dec << "] " \
                    << severity << simplify_pretty(__PRETTY_FUNCTION__) << ": "
#else
#define LOG(severity)                          \
  if (severity >= tools::log::severity_filter) \
  tools::log::log() << severity << simplify_pretty(__PRETTY_FUNCTION__) << ": "
#endif

namespace tools {
namespace log {

// Log only severities at the same level or more severe than the severity_filter
extern enum LOG_LEVEL severity_filter;

// Synchronize the log output among multiple threads
extern std::mutex log_lock;

// inline before moved somewhere else
inline std::ostream &operator<<(std::ostream &os, enum LOG_LEVEL severity) {
  switch (severity) {
    case TRACE:
      return os << "<7> ";
    case DEBUG:
      return os << "<7> ";
    case INFO:
      return os << "<6> ";
    case WARNING:
      return os << "<4> ";
    case ERROR:
      return os << "<3> ";
    case FATAL:
      return os << "<1> ";
  }
  return os;
}

inline void set_filter_min_severity(enum LOG_LEVEL filter) { severity_filter = filter; }

// From:
// https://stackoverflow.com/questions/2179623/how-does-qdebug-stuff-add-a-newline-automatically/2179782#2179782

struct log {
  log() = default;

  ~log() {
    std::lock_guard<std::mutex> guard(log_lock);
    std::cout << os.str() << std::endl;
  }

 public:
  // accepts just about anything
  template <class T>
  log &operator<<(const T &x) {
    os << x;
    return *this;
  }

 private:
  std::ostringstream os;
};

}  // namespace log
}  // namespace tools
