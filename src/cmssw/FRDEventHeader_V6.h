#ifndef FRDEventHeader_V6_H
#define FRDEventHeader_V6_H

class FRDEventHeader_V6 {
 public:
  FRDEventHeader_V6(uint16_t version, uint16_t flags, uint32_t run, uint32_t lumi, uint32_t event,
                    uint32_t eventSize, uint32_t crc32c) {
    version_ = version;
    flags_ = flags;
    run_ = run;
    lumi_ = lumi;
    event_ = event;
    eventSize_ = eventSize;
    crc32c_ = crc32c;
  }

 private:
  uint16_t version_;
  uint16_t flags_;
  uint32_t run_;
  uint32_t lumi_;
  uint32_t event_;
  uint32_t eventSize_;
  uint32_t crc32c_;
};

#endif
