#ifndef FRDFileHeader_v2_H
#define FRDFileHeader_v2_H
#include <array>
// Straight from CMSSW
constexpr std::array<unsigned char, 4> FRDFileHeader_id{{0x52, 0x41, 0x57, 0x5f}};
constexpr std::array<unsigned char, 4> FRDFileVersion_2{{0x30, 0x30, 0x30, 0x32}};

class FRDFileHeader_v2 {
 public:
  FRDFileHeader_v2(uint16_t headerSize, uint16_t dataType, uint32_t eventCount, uint32_t runNumber,
                   uint32_t lumiSection, uint64_t fileSize) {
    id_ = FRDFileHeader_id;
    version_ = FRDFileVersion_2;
    headerSize_ = headerSize;
    dataType_ = dataType;
    eventCount_ = eventCount;
    runNumber_ = runNumber;
    lumiSection_ = lumiSection;
    fileSize_ = fileSize;
  }
  uint64_t getFileSize() { return fileSize_; }

 private:
  std::array<unsigned char, 4> id_;
  std::array<unsigned char, 4> version_;
  uint16_t headerSize_;
  uint16_t dataType_;
  uint32_t eventCount_;
  uint32_t runNumber_;
  uint32_t lumiSection_;
  uint64_t fileSize_;
};

#endif
