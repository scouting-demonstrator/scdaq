#ifndef OUTPUTFILE_H
#define OUTPUTFILE_H

#include <tbb/concurrent_queue.h>

#include <atomic>
#include <cstdint>
#include <cstdio>
#include <fstream>
#include <iostream>
#include <string>
#include <thread>
#include <utility>
#include <vector>

#include "cmssw/FRDFileHeader_v2.h"

class OutputFile {
 public:
  OutputFile(FILE* filePtr, std::string fileName, FRDFileHeader_v2 fileHeader, uint32_t lumisection,
             uint32_t index, std::string rundir)
      : filePtr_(filePtr),
        fileName_(fileName),
        fileHeader_(fileHeader),
        lumisection_(lumisection),
        index_(index),
        rundir_(rundir) {
    fileSize_ = 0;
    NOrbits_ = 0;
  }
  OutputFile() : filePtr_(NULL), fileName_(""), fileHeader_(0, 0, 0, 0, 0, 0) {}
  std::string getFileName() { return fileName_; }
  FILE* getFilePtr() { return filePtr_; }
  FRDFileHeader_v2 getFileHeader() { return fileHeader_; }
  std::string getRunDir() { return rundir_; }
  bool writeFileHeader() {
    fseek(filePtr_, 0, SEEK_SET);
    size_t written = fwrite(&fileHeader_, 1, sizeof(fileHeader_), filePtr_);
    if (written == sizeof(fileHeader_)) {
      return 1;
    } else {
      return 0;
    }
  }

  void nullify() {
    filePtr_ = 0;
    fileName_ = std::string();
  }

  bool exists() {
    if (filePtr_ == NULL) {
      return 0;
    } else {
      return 1;
    }
  }

  void setFileHeader(FRDFileHeader_v2 fileHeader) { fileHeader_ = fileHeader; }

  uint32_t Lumisection() { return lumisection_; }
  uint32_t Index() { return index_; }

  size_t getFileSize() { return fileSize_; }
  uint32_t getNOrbits() { return NOrbits_; }

  void setFileSize(size_t fileSize) { fileSize_ = fileSize; }
  void setNOrbits(uint32_t NOrbits) { NOrbits_ = NOrbits; }

 private:
  FILE* filePtr_;
  std::string fileName_;
  FRDFileHeader_v2 fileHeader_;
  uint32_t lumisection_;
  uint32_t index_;
  std::string rundir_;
  size_t fileSize_;
  uint32_t NOrbits_;
};

#endif