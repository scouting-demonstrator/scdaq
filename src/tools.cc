#include "tools.h"

#include <mutex>

#include "log.h"

namespace tools {
namespace log {

/* Definitions of forward declarations from log.h */
enum LOG_LEVEL severity_filter;
std::mutex log_lock;

}  // namespace log
}  // namespace tools

/*
 * This function is just used to silence libcurl (see below)
 */
static size_t WriteCallback(void *, size_t size, size_t nmemb, void *) { return size * nmemb; }

/*
 * Helper function to reset the board via SCONE
 */
int tools::reset_board(std::string host, std::string port, std::string board) {
  std::string url{"http://" + host + ":" + port + "/" + board + "/reset_board/write"};
  CURL *curl;
  curl = curl_easy_init();
  if (curl) {
    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
    curl_easy_setopt(curl, CURLOPT_FAILONERROR, 1);
    curl_easy_setopt(curl, CURLOPT_POST, 1);
    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "value=1");  // Set reset to high
    // To suppress the command line output of received data we need to pass in
    // this callback function.
    std::string dummy;
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &dummy);
    CURLcode res = curl_easy_perform(curl);
    if (res != 0) {
      return res;
    }
    sleep(1);
    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "value=0");  // Deassert reset
    res = curl_easy_perform(curl);
    if (res != 0) {
      return res;
    }
    curl_easy_cleanup(curl);
  } else {
    return -1;
  }
  return 0;
}
