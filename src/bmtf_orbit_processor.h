#ifndef BMTF_PROCESSOR_H
#define BMTF_PROCESSOR_H

#include "orbit_processor.h"

class BmtfOrbitProcessor : public OrbitProcessor {
  using OrbitProcessor::OrbitProcessor;

 protected:
  int ProcessBlock(MemRegion &readable_block, WriteMemRegion &writeable_block,
                   BxMetadata meta) override;

  inline uint32_t GetPacketSize() const override {
    // size of bmtf stubs packet is 4bytes*(3headerWords + 2frames*8dataWords)=236
    return 4 * (3 + (2 * 8));
  }

  inline uint32_t GetOrbitSizeInBytes(const FillOrbitMetadata &meta) const override {
    // Each BX requires 3 words (12 bytes); each stub takes 8 bytes
    return 12 * meta.filled_bxs + 8 * meta.counts;
  }
};

#endif  // BMTF_PROCESSOR_H