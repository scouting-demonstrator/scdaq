#include "orbit_processor.h"

#include <cassert>
#include <cstring>
#include <utility>

#include "cmssw/FRDEventHeader_V6.h"
#include "format.h"
#include "log.h"

// checks that the packet size is an integer multiple of the BX block size, minus the
// header/trailers
bool OrbitProcessor::CheckFrameMultBlock(size_t inputSize, uint16_t nDroppedOrbitsInPacket) const {
  if ((inputSize - (nOrbitsPerPacket - nDroppedOrbitsInPacket) * constants::orbit_trailer_size -
       32 * (1 + static_cast<int>(dthHeaders)) * nOrbitsPerPacket -
       32 * static_cast<int>(dthHeaders) - 32) %
          bsize !=
      0) {
    stats.n_consistent_sized_packets = 0;
    stats.packet_skipped_inconsistent_size++;

    if ((stats.packet_skipped_inconsistent_size++ == 1) ||
        ((stats.packet_skipped_inconsistent_size < 100) &&
         (stats.packet_skipped_inconsistent_size % 10 == 0)) ||
        ((stats.packet_skipped_inconsistent_size < 1000) &&
         (stats.packet_skipped_inconsistent_size % 100 == 0)) ||
        ((stats.packet_skipped_inconsistent_size < 10000) &&
         (stats.packet_skipped_inconsistent_size % 1000 == 0)) ||
        (stats.packet_skipped_inconsistent_size % 10000 == 0)) {
      LOG(WARNING) << "Frame size not a multiple of block size after headers "
                      "and trailers have been subtracted. Counted "
                   << stats.packet_skipped_inconsistent_size << " packets skipped.";
    }

    LOG(WARNING) << "Frame size not a multiple of block size after orbit headers "
                    "(32B*nOrbitsPerPacket), orbit trailers (512B*nOrbitsPerPacket), "
                    "and packet trailer (32B) have been subtracted. \n Frame size = "
                 << inputSize << ", block size = " << bsize << ", packet will be skipped";
    return false;
  } else {
    stats.n_consistent_sized_packets++;
    if (stats.n_consistent_sized_packets == 6000000) {  // every ~10 mins
      LOG(WARNING) << "Resetting packet_skipped_inconsistent_size counter to 0 "
                      "after 6000000M consistent packets. Count was at "
                   << stats.packet_skipped_inconsistent_size;
      stats.packet_skipped_inconsistent_size = 0;
    }
  }
  return true;
}

bool OrbitProcessor::HasTrailer(Slice &input, char *&rd_ptr) const {
  rd_ptr += 32 + 32 * static_cast<int>(dthHeaders);  // +32 to account for orbit header
  while (rd_ptr + sizeof(orbit_trailer) - 1 <= input.end()) {
    auto *ot = reinterpret_cast<orbit_trailer *>(rd_ptr);
    if (ot->beefdead[0] == constants::beefdead) {  // found orbit trailer
      return true;
    }
    rd_ptr += bsize;
  }
  return false;
}

std::pair<uint32_t, bool> OrbitProcessor::ProcessOrbitHeader(char *rd_ptr) {
  // get orbit from orbit header
  auto *bl_pre =
      reinterpret_cast<blockMuon *>(rd_ptr);  // blockMuon.orbit is identical to blockCalo.frame0
  auto orbitN = uint32_t{bl_pre->orbit[0]};
  // save warning_test_enable bit
  auto mask = uint32_t(1) << 31;
  bool warning_test_enable = ((orbitN & mask) == mask);
  // remove warning_test_enable bit from orbit header
  orbitN &= 0x7fffffff;
  return {orbitN, warning_test_enable};
}

// add to header. allocate new[]
size_t OrbitProcessor::fillFRDEventHeader_V6(char *wr_ptr_FRDHead, uint32_t input_size,
                                             uint32_t orbit) const {
  const uint16_t header_version = 6;
  const uint16_t flags = 0;
  const uint32_t crc_dummy = 0;
  // Source ID is scouting-specific, thus not intrinsically a part of FRDEventHeader_V6
  // NOTE: for now, 32 extra bits must be reserved in the event size for the source ID, even if it
  // is a 16-bit integer
  const uint32_t source_id = static_cast<uint32_t>(source_id_);
  const uint32_t event_size = input_size + static_cast<uint32_t>(sizeof(source_id));

  const uint32_t lumisection =
      1 + static_cast<uint32_t>(orbit / constants::N_orbits_per_lumisection);
  FRDEventHeader_V6 frdEventHeader_V6(header_version, flags, control_.run_number, lumisection,
                                      orbit, event_size, crc_dummy);
  memcpy(wr_ptr_FRDHead, (char *)&(frdEventHeader_V6), sizeof(frdEventHeader_V6));
  wr_ptr_FRDHead += sizeof(frdEventHeader_V6);
  memcpy(wr_ptr_FRDHead, (char *)&(source_id), sizeof(source_id));

  return (sizeof(frdEventHeader_V6) + sizeof(source_id));
}

// Fills array pointed by `wr_ptr` with an orbit's data corresponding to non-empty bunch-crossings,
// as marked in `bx_vect`
OrbitProcessor::FillOrbitMetadata OrbitProcessor::FillOrbit(orbit_trailer *trailer, char *rd_ptr,
                                                            char *wr_ptr, const char *rd_end_ptr,
                                                            const char *wr_end_ptr,
                                                            bool is_dropped_orbit) {
  std::pair<uint32_t, bool> orbit_header = ProcessOrbitHeader(rd_ptr);
  auto orbit = static_cast<int>(orbit_header.first);
  if (orbit <= 0) {
    return {0, 0, 0};
  }
  //.second is the warning test enable bit
  rd_ptr += 32;  // +32 to account for orbit header
  if (cmsswHeaders) {
    wr_ptr += sizeof(FRDEventHeader_V6) + 4;
  }  // reserving space for cmssw orbit header
  if (is_dropped_orbit) {
    return {0, orbit, 0};
  }
  auto counts = uint32_t{0};
  uint32_t filled_bxs = 0;
  // We loop over the BX map from the orbit trailer and then match the filled
  // BXs to the data we got.
  for (size_t bx = 1; bx <= 3564; ++bx) {
    // firstly check if we have BX data in orbit
    if (CheckOrbitTrailerStart(const_cast<const char *>(rd_ptr))) {
      // orbit trailer has been reached, end of orbit data
      break;
    }

    // update word and bit...
    const size_t word = (bx - 1) / 32;
    const size_t bit = (bx - 1) % 32;
    // ...and check if BX is empty...
    if ((trailer->bx_map[word] & (1 << bit)) == 0) {
      continue;
    }
    // ...otherwise we have something
    ++filled_bxs;

    // fill
    MemRegion readable_block(&rd_ptr, rd_end_ptr);
    WriteMemRegion writeable_block(&wr_ptr, wr_end_ptr);

    LOG(DEBUG) << "Writing to memory region: " << std::to_string(writeable_block.GetAvailable())
               << " bytes available, " << std::to_string(GetPacketSize()) << " needed.";
    assert(writeable_block.CheckBounds(GetPacketSize()));  // Max size a decoded block can use

    BxMetadata meta{orbit_header.second, static_cast<uint32_t>(bx), orbit};

    const int ret_counts = ProcessBlock(readable_block, writeable_block, meta);
    if (ret_counts == -1) {
      continue;  // skip bunch crossing
    }
    counts += ret_counts;
  }
  return {counts, orbit, filled_bxs};
}

void OrbitProcessor::ProcessSliceImpl(Slice &input, Slice &out) {
  char *rd_ptr = input.begin();
  char *wr_ptr = out.begin();
  char *rd_end_ptr = input.end();
  char *wr_end_ptr = out.begin() + out.avail();
  uint32_t counts = 0;
  uint32_t orbit_per_packet_count = 0;
  bool firstOrbit = true;
  uint16_t n_dropped_orbits_in_packet = 0;
  orbit_trailer *trailer = nullptr;
  packet_header *ph_header = nullptr;    // packet header frame
  fragment_header *fh_header = nullptr;  // fragment header frame
  bool is_dropped_orbit = false;

  FillOrbitMetadata meta{0, 0, 0};

  if (dthHeaders) {
    ph_header = reinterpret_cast<packet_header *>(rd_ptr);
    n_dropped_orbits_in_packet = ph_header->dropped_orbit_in_packet;
    rd_ptr += 32;  // 0; // commented for fastTcpRead implementation
  }

  if (!CheckFrameMultBlock(input.size(), n_dropped_orbits_in_packet)) {
    return;
  }

  while (true) {
    uint32_t orbitCount = 0;
    char *trailer_ptr = rd_ptr;

    if (dthHeaders) {
      fh_header = reinterpret_cast<fragment_header *>(rd_ptr);
      is_dropped_orbit = (fh_header->flag_dropped_orbit == 1);
      rd_ptr += 32;
    }

    if (!is_dropped_orbit) {
      const bool trailerFound = HasTrailer(input, trailer_ptr);

      if (!trailerFound) {
        stats.orbit_trailer_error_count++;
        LOG(WARNING) << "Orbit trailer error: orbit trailer not found before end of data "
                        "packet. Packet will be skipped. Orbit trailer error count = "
                     << stats.orbit_trailer_error_count;
        return;
      }
      trailer = reinterpret_cast<orbit_trailer *>(trailer_ptr);
    }
    size_t additional_header_size = 0;

    meta = FillOrbit(trailer, rd_ptr, wr_ptr, rd_end_ptr, wr_end_ptr, is_dropped_orbit);
    if (meta.orbit <= 0) {
      LOG(WARNING) << "Invalid orbit number " << meta.orbit << ". Skipping packet...";
      return;
    }
    orbitCount = meta.counts;
    ++orbit_per_packet_count;

    // NOTE: Only relevant for GMT processor, as number of orbits is always 1 in the CALO case.
    if (orbit_per_packet_count > nOrbitsPerPacket) {
      stats.excess_orbits_per_packet_count++;
      return;
    }

    uint32_t orbit_size_bytes = GetOrbitSizeInBytes(meta);

    if (cmsswHeaders) {
      additional_header_size = fillFRDEventHeader_V6(wr_ptr, orbit_size_bytes, meta.orbit);
    }
    wr_ptr += (orbit_size_bytes + additional_header_size);

    rd_ptr += 32 + meta.filled_bxs * bsize +
              constants::orbit_trailer_size *
                  static_cast<int>(!is_dropped_orbit);  // 32 for orbit header, +
                                                        // nBXs + orbit trailer
    assert(wr_ptr <= wr_end_ptr);
    assert(rd_ptr <= rd_end_ptr);
    counts += orbitCount;
    if (firstOrbit) {
      out.set_firstOrbitN(meta.orbit);
      firstOrbit = false;
    }

    if (rd_ptr < input.end()) {
      auto *dma_trailer_word = reinterpret_cast<uint32_t *>(rd_ptr);
      if (*dma_trailer_word == constants::deadbeef) {
        out.set_end(wr_ptr);
        out.set_counts(counts);
        return;
      }

      if (orbit_per_packet_count > nOrbitsPerPacket) {
        LOG(WARNING) << "expected DMA trailer word deadbeef, found " << std::hex
                     << *dma_trailer_word << ". Orbits per packet count " << orbit_per_packet_count
                     << ", > expected, (" << nOrbitsPerPacket << ") skipping packet.";
        if (stats.excess_orbits_per_packet_count % 10000 == 0) {
          LOG(WARNING) << "count of packets with excess # orbits "
                       << stats.excess_orbits_per_packet_count;
        }
        return;
      }
    }
  }
}
