#ifndef FILE_DMA_INPUT_FILTER_H
#define FILE_DMA_INPUT_FILTER_H

#include <tbb/pipeline.h>
#include <tbb/tick_count.h>

#include <memory>
#include <string>

#include "InputFilter.h"

class FileDmaInputFilter : public InputFilter {
 public:
  FileDmaInputFilter(const std::string &filename, size_t packetBufferSize, size_t nbPacketBuffers,
                     ctrl &control);
  ~FileDmaInputFilter() override;

 protected:
  ssize_t readInput(char **buffer, size_t bufferSize) override;
  void print(std::ostream &out) const override;

 private:
  ssize_t readPacket(char **buffer, ssize_t bufferSize);

 private:
  FILE *inputFile;

  struct Statistics {
    uint64_t nbOversizedPackets = 0;
  } stats;
};

#endif  // FILE_DMA_INPUT_FILTER_H
