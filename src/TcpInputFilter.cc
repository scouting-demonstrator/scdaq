#include "TcpInputFilter.h"

#include <sys/socket.h>
#include <unistd.h>

#include <cerrno>
#include <cstdio>
#include <iostream>
#include <system_error>

#include "format.h"
#include "log.h"

TcpInputFilter::TcpInputFilter(int dest_port, size_t packet_buffer_size, size_t num_packet_buffers,
                               const uint32_t num_orbits_per_packet, ctrl &control)
    : InputFilter(packet_buffer_size, num_packet_buffers, control),
      nOrbitsPerPacket_(num_orbits_per_packet),
      destPort_(dest_port) {
  // open socket
  if ((sock_ = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
    throw std::system_error(errno, std::system_category(), "Error opening socket.");
  }

  // set SO_REUSEADDR on a socket to bypass TIME_WAIT state
  const int enable = 1;
  if (setsockopt(sock_, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) == -1) {
    throw std::system_error(errno, std::system_category(),
                            "Cannot set socket option to bypass TIME_WAIT state.");
  }

  // server socket address
  server_addr_.sin_family = AF_INET;
  server_addr_.sin_addr.s_addr = INADDR_ANY;
  server_addr_.sin_port = htons(destPort_);
  bzero(&(server_addr_.sin_zero), 8);

  // bind socket to address
  if (bind(sock_, (struct sockaddr *)&server_addr_, sizeof(struct sockaddr)) == -1) {
    throw std::system_error(errno, std::system_category(), "Unable to bind.");
  }

  // check if listen is possible
  if (listen(sock_, 1) == -1) {
    throw std::system_error(errno, std::system_category(), "Unable to listen.");
  }

  LOG(TRACE) << "Created TCP input filter";
}

TcpInputFilter::~TcpInputFilter() {
  close(connected_);
  LOG(TRACE) << "Destroyed TCP input filter";
}

static inline ssize_t read_tcp_socket_to_buffer(int sock, char *buffer, uint64_t size) {
  ssize_t rc;
  uint64_t bytesRecvd = 0;

  // get data from socket until requested size is reached
  while (bytesRecvd < size) {
    rc = recv(sock, buffer + bytesRecvd, size - bytesRecvd, 0);

    // if connection is closed, recv return value is negative
    // return the negative result to signal it
    if (rc < 0) {
      return rc;
    }
    if (rc == 0) {
      throw std::runtime_error("ERROR: TCP connection was unexpectedly closed by peer.");
    }

    bytesRecvd += rc;
  }

  return bytesRecvd;
}

// open connection
void TcpInputFilter::openConnection() {
  LOG(TRACE) << "Waiting for connection to be opened";
  sin_size_ = sizeof(struct sockaddr_in);
  connected_ = accept(sock_, (struct sockaddr *)&client_addr_, &sin_size_);

  if (connected_ < 0) {
    connection_open_ = false;
    throw std::system_error(errno, std::system_category(),
                            "Error during opening of the connection.");
  }

  LOG(TRACE) << "Connection opened";
  connection_open_ = true;
}

// close connection
void TcpInputFilter::closeConnection() {
  if (connection_open_) {
    close(connected_);
    LOG(TRACE) << "Connection closed";
  }

  connection_open_ = false;
}

// read packet from TCP socket expecting header frame
// + header frame has to be the first one received
// + extract the payload size encoded in the header frame
// + the logic receives until the given size is reached
ssize_t TcpInputFilter::readPacketFromTcp(char **buffer, size_t bufferSize) {
  // start to read header block size
  // then update read size from header content
  ssize_t blockSize = 32;
  ssize_t bytesRecvd = 0;
  ssize_t rc;

  // header frame
  packet_header *header = nullptr;

  // if connection is still closed, open it
  if (!connection_open_) {
    openConnection();
  }

  bool firstRead = true;
  while (true) {
    // receive requested size from TCP connected socket
    rc = read_tcp_socket_to_buffer(connected_, *buffer + bytesRecvd, blockSize);

    // if recv gave negative results, restart connection
    if (rc < 0) {
      closeConnection();
      openConnection();
      // avoid the case where:
      // + header is received and blockSize decoded
      // + connection is closed and HBM pointers have been reset on FPGA board
      // + receive invalid blockSize
      // thus, be ready to receive a new header + payload
      blockSize = 32;
      bytesRecvd = 0;
      firstRead = true;
      continue;
    }

    // update bytes received counter
    bytesRecvd += rc;

    // check if we are at the first read (header) or not
    if (firstRead) {
      // interpret header
      header = reinterpret_cast<packet_header *>(*buffer);

      // check if header marker is in the frame
      // if not, try again N times
      if (header->feedbeef != 0xfeedbeeffeedbeef) {
        printf("Frame   0x%16lu_%16u_%16u_%16u\n", header->feedbeef, header->frames_in_packet,
               header->orbit_dropped_counter, header->orbit_seen_counter);
        stats.nbHeaderDesync++;
        throw std::runtime_error("FATAL: Header not found.");
      }

      // header found, disable firstRead flag and get packet size
      firstRead = false;
      blockSize = (header->frames_in_packet + nOrbitsPerPacket_ + 1) * 32;

      if (blockSize > (ssize_t)bufferSize) {
        stats.nbOversizedPackets++;
      }
    } else {
      // packet has been read, return bytes in packet
      return bytesRecvd;
    }
  }
}

/**************************************************************************
 * Entry points are here
 * Overriding virtual functions
 */

// Print some additional info
void TcpInputFilter::print(std::ostream &out) const {
  out << ", oversized " << stats.nbOversizedPackets << ", scouting packet header desync "
      << stats.nbHeaderDesync;
}

// Read a packet from TCP socket
ssize_t TcpInputFilter::readInput(char **buffer, size_t bufferSize) {
  // call function with header read and blocks breaker
  return readPacketFromTcp(buffer, bufferSize);
}
