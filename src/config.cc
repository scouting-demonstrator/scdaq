#include "config.h"

#include <cctype>
#include <fstream>
#include <functional>
#include <iostream>
#include <string>
#include <string_view>

#include "json5cpp.h"
#include "log.h"

namespace tools {
bool ends_with(std::string_view str, std::string_view suffix) {
  return str.size() >= suffix.size() &&
         str.compare(str.size() - suffix.size(), suffix.size(), suffix) == 0;
}
}  // namespace tools

template <>
int ConfigMap::Parse<ConfigMap::EFormat::JSON5>(std::ifstream &input) {
  ValueType value;
  std::string err;
  if (Json5::parse(input, value, &err)) {
    Import(value);
  } else {
    LOG(ERROR) << "Invalid JSON5: " << err;
    return -1;
  }
  return 0;
}

ConfigMap::ConfigMap(std::string_view filename) {
  const auto filename_str = std::string(filename);
  // Open file and check if valid
  std::ifstream in(filename_str, std::ios_base::in);
  if (!in.is_open()) {
    throw std::invalid_argument("Configuration file (" + filename_str +
                                ") could not be opened. Does it exist?");
  }

  // Pass open file handle to parser according to filename ending
  if (tools::ends_with(filename_str, ".json5")) {
    Parse<ConfigMap::EFormat::JSON5>(in);
  } else if (tools::ends_with(filename_str, ".conf")) {
    throw std::invalid_argument(
        "Configuration file (" + filename_str +
        ") has `conf` suffix. Please use JSON5, conforming to the templates in `test/config`.");
  } else {
    throw std::invalid_argument("Configuration file (" + filename_str +
                                ") has unrecognized suffix. Please specify the intended format.");
  }
}

Config::Config(std::string_view filename)
    : dict_(filename),
      // Setup configs
      input_type_(ToInputType(dict_.Get("input"))),
      scone_address_(dict_.Get<SconeAddress>("scone_address")),
      dma_device_(dict_.Get("dma_dev")),
      port_(dict_.Get<int>("port")),
      num_threads_(dict_.Get<uint32_t>("threads")),
      // Logging, statistics, debugging
      attempt_tcp_reconnect_(dict_.Get<bool>("dev_TCPAutoReconnectOnFailure")),
      num_packets_per_report_(dict_.Get<uint32_t>("packets_per_report")),
      num_packet_buffers_(dict_.Get<uint64_t>("dma_number_of_packet_buffers")),
      packet_buffer_size_(dict_.Get<uint64_t>("dma_packet_buffer_size")),
      min_log_severity_(ToLOG_LEVEL(dict_.Get("log_min_severity"))),
      // Run input configs
      num_orbits_per_packet_(dict_.Get<uint64_t>("nOrbitsPerPacket")),
      input_files_(dict_.GetArray<std::string>("input_files")),
      input_streams_(dict_.GetArray<StreamConfig>("input_streams")),
      // Run processor configs
      enable_processing_(dict_.Get<bool>("enable_stream_processor")),
      apply_zero_suppression_(dict_.Get<bool>("doZS")),
      // Run output configs
      output_file_base_(dict_.Get("output_filename_base")),
      output_file_prefix_(dict_.Get("output_filename_prefix")),
      output_file_suffix_(dict_.Get("output_filename_suffix")),
      force_write_out_(dict_.Get<bool>("output_force_write")),
      max_file_size_(dict_.Get<uint64_t>("max_file_size")),
      support_cmssw_headers_(dict_.Get<bool>("cmsswHeaders")),
      support_dth_headers_(dict_.Get<bool>("dthHeaders")),
      // if num_orbits_per_file_ > 0, uses fixed N orbits per file
      num_orbits_per_file_(dict_.Get<uint64_t>("nOrbitsPerFile")) {}

const StreamConfig &Config::GetStreamConfigById(uint32_t stream_id) const {
  return input_streams_[stream_id];
}

const std::string &Config::GetInputFileById(uint32_t stream_id) const {
  return input_files_[stream_id];
}
