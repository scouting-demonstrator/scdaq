#ifndef MUON_ORBIT_PROCESSOR_H
#define MUON_ORBIT_PROCESSOR_H

#include "orbit_processor.h"

class MuonOrbitProcessor : public OrbitProcessor {
  using OrbitProcessor::OrbitProcessor;

 protected:
  int ProcessBlock(MemRegion &readable_block, WriteMemRegion &writeable_block,
                   BxMetadata meta) override;

  inline uint32_t GetPacketSize() const override { return (3 + 8 * 3) * 4; }

  inline uint32_t GetOrbitSizeInBytes(const FillOrbitMetadata &meta) const override {
    // 12 bytes for each muon/count then 12 bytes for each bx header
    return meta.counts * 12 + 12 * meta.filled_bxs;
  }
};

#endif  // MUON_ORBIT_PROCESSOR_H