#ifndef CALO_SUM_PROCESSOR_H
#define CALO_SUM_PROCESSOR_H

#include "orbit_processor.h"

class CaloSumOrbitProcessor : public OrbitProcessor {
  using OrbitProcessor::OrbitProcessor;

 protected:
  int ProcessBlock(MemRegion &readable_block, WriteMemRegion &writeable_block,
                   BxMetadata meta) override;

  inline uint32_t GetPacketSize() const override {
    // size of calo sums packet is 4bytes*(2headerWords + 6sums)=32
    return 4 * (2 + 6);
  }

  inline uint32_t GetOrbitSizeInBytes(const FillOrbitMetadata &meta) const override {
    return GetPacketSize() * meta.filled_bxs;
  }
};

#endif  // CALO_SUM_PROCESSOR_H