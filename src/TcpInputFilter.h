#ifndef TCP_INPUT_H
#define TCP_INPUT_H

#include <arpa/inet.h>
#include <netinet/in.h>
#include <tbb/pipeline.h>
#include <tbb/tick_count.h>

#include <memory>
#include <string>

#include "InputFilter.h"
#include "config.h"

class TcpInputFilter : public InputFilter {
 public:
  TcpInputFilter(int dest_port, size_t packet_buffer_size, size_t num_packet_buffers,
                 const uint32_t num_orbits_per_packet, ctrl &control);
  ~TcpInputFilter() override;

 protected:
  ssize_t readInput(char **buffer, size_t bufferSize) override;
  void print(std::ostream &out) const override;

 private:
  void openConnection();
  void closeConnection();
  ssize_t readPacketFromTcp(char **buffer, size_t bufferSize);

  struct Statistics {
    uint64_t nbOversizedPackets = 0;
    uint64_t nbHeaderDesync = 0;
  } stats;

  const uint32_t nOrbitsPerPacket_;

  sockaddr_in server_addr_{};
  sockaddr_in client_addr_{};
  int destPort_;
  int sock_{0};
  unsigned int sin_size_{0};
  int connected_{0};
  bool connection_open_{false};
};

#endif
