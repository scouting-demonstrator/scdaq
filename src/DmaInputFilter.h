#ifndef DMA_INPUT_H
#define DMA_INPUT_H

#include <tbb/pipeline.h>
#include <tbb/tick_count.h>

#include <memory>
#include <string>

#include "InputFilter.h"

class DmaInputFilter : public InputFilter {
 public:
  DmaInputFilter(const std::string &deviceFileName, size_t packetBufferSize, size_t nbPacketBuffers,
                 ctrl &control);
  ~DmaInputFilter() override;

 protected:
  ssize_t readInput(char **buffer, size_t bufferSize) override;
  void print(std::ostream &out) const override;

 private:
  int dma_fd;

  ssize_t readPacketFromDMA(char **buffer, size_t bufferSize);

  struct Statistics {
    uint64_t nbDmaErrors = 0;
    uint64_t nbDmaOversizedPackets = 0;
  } stats;
};

#endif
