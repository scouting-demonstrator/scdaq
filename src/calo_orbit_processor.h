#ifndef CALO_PROCESSOR_H
#define CALO_PROCESSOR_H

#include "orbit_processor.h"

class CaloOrbitProcessor : public OrbitProcessor {
  using OrbitProcessor::OrbitProcessor;

 protected:
  int ProcessBlock(MemRegion &readable_block, WriteMemRegion &writeable_block,
                   BxMetadata meta) override;

  inline uint32_t GetPacketSize() const override {
    // size of calo packet is 4bytes*(8links*7dataWords + 3headerWords)=236 bytes.
    // Note 7 data words per link because we have the "link number" word + 6 words from calo L2
    return 4 * ((8 * 7) + 3);
  }

  inline uint32_t GetOrbitSizeInBytes(const FillOrbitMetadata &meta) const override {
    return GetPacketSize() * meta.filled_bxs;
  }
};

#endif  // CALO_PROCESSOR_H