#ifndef TOOLS_H
#define TOOLS_H

/*
 * Some useful functions
 */

#include <curl/curl.h>
#include <sys/stat.h>
#include <unistd.h>  // TODO: This could eventually be replaced by chrono and thread when we move to a more modern C++ compiler.

#include <climits>
#include <cstring>
#include <iostream>

namespace tools {

/*
 * A thread-safe version of strerror: Returns a C++ std::string describing
 * error_code
 */
inline std::string strerror(int error_code) {
  char local_buf[128];
  char *buf = local_buf;
  // NOTE: The returned buf may not be the same as the original buf
  buf = strerror_r(error_code, buf, sizeof(local_buf));
  // Return a proper C++ string out of the buffer
  return {buf};
}

/*
 * A thread-safe version of strerror: Returns a C++ std::string describing ERRNO
 */
inline std::string strerror() { return tools::strerror(errno); }

/*
 * A thread-safe version of "perror". Prepends a custom message before the error
 * code and returns everything in std::string
 */
inline std::string strerror(const std::string &msg) { return msg + ": " + tools::strerror(); }

/*
 * Helper function to reset the board via SCONE
 */
int reset_board(std::string host, std::string port, std::string board);

/*
 * Various filesystem related utilities (will be removed once moved to C++17, or
 * rewritten with boost)
 */
namespace filesystem {

/*
 * Create the target directory and any parent directories if necessary
 */
inline bool create_directories(std::string &path) {
  char tmp[PATH_MAX];

  // Add terminating '/' and make a writable copy;
  int len = snprintf(tmp, sizeof(tmp), "%s/", path.c_str());
  if (len > PATH_MAX) len = PATH_MAX;

  char *last_backslash = tmp;
  for (char *p = tmp; *p; p++) {
    if (*p == '/') {
      // Found a new directory, ignore any subsequent backslashes
      auto dir_len = p - last_backslash - 1;
      if (dir_len > 0) {
        *p = 0;
        if (mkdir(tmp, S_IRWXU | S_IRWXG | S_IRWXO) < 0 && (errno != EEXIST)) {
          return false;
        }
        *p = '/';
      }
      last_backslash = p;
    }
  }

  return true;
}

}  // namespace filesystem
}  // namespace tools

#endif  // TOOLS_H
