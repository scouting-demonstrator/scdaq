#ifndef PROCESSOR_H
#define PROCESSOR_H

#include <tbb/pipeline.h>

#include <atomic>

#include "slice.h"

class Processor : public tbb::filter {
 public:
  Processor();
  ~Processor() override = default;

  // TBB functor override
  void *operator()(void *item) override;

  static class Statistics {
   public:
    Statistics()
        : orbit_trailer_error_count(0),
          packet_count(0),
          excess_orbits_per_packet_count(0),
          packet_skipped_inconsistent_size(0),
          n_consistent_sized_packets(0) {}

    std::atomic<uint32_t> orbit_trailer_error_count;
    std::atomic<uint64_t> packet_count;
    std::atomic<uint64_t> excess_orbits_per_packet_count;
    std::atomic<uint64_t> packet_skipped_inconsistent_size;
    std::atomic<uint64_t> n_consistent_sized_packets;
  } stats;

 protected:
  void ProcessSlice(Slice &input, Slice &out);

  uint64_t nbPackets;

  virtual void ProcessSliceImpl(Slice &input, Slice &out);
};

#endif  // PROCESSOR_H