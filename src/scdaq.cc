#include <iostream>
#include <vector>

#include "config.h"
#include "log.h"
#include "pipeline.h"

ctrl Pipeline::control_;

int main(int argc, char *argv[]) {
  (void)(argc);
  (void)(argv);

  if (argc < 2) {
    LOG(DEBUG) << "no arguments provided to scdaq, try --help";
    return 1;
  }

  if ((std::string(argv[1]) == "-h") || (std::string(argv[1]) == "--help")) {
    LOG(DEBUG) << "HELP: expected arguments --config [config basename] --nstreams [number of "
                  "parallel streams]";
    return 1;
  }

  if ((argc < 3)) {
    LOG(ERROR) << "error occurred, number of arguments is less than 2, expected --config [config "
                  "basename] (and optionally --nstreams [number of parallel streams]), try --help";
    return 1;
  }

  std::string config_basename{};

  if (std::string(argv[1]) == "--config") {
    LOG(DEBUG) << "scdaq started with config files basename:   " << std::string(argv[2]);
    config_basename.assign(argv[2]);
  } else {
    LOG(ERROR) << "invalid argument, expected --config, see --help";
    return 1;
  }
  // Instantiate multiple pipelines with the settings from the config instances.
  MultiPipeline multi_pipeline(Config{config_basename});
  int ret;
  try {
    // Launch blocking call to run the pipelines in parallel until all streams are severed
    ret = multi_pipeline.Run();
  } catch (std::exception &e) {
    LOG(ERROR) << "Error in pipelines. Error text is: \"" << e.what() << "\"";
    return 1;
  }
  return ret;
}
