#include "muon_orbit_processor.h"

#include <cassert>

#include "log.h"

int MuonOrbitProcessor::ProcessBlock(MemRegion &readable_block, WriteMemRegion &writeable_block,
                                     BxMetadata meta) {
  struct MuonLinkData {
    uint32_t f;
    uint32_t s;
    uint32_t extra;
  };

  using SourceDataType = blockMuon;
  using SinkDataType = MuonLinkData;
  const uint32_t source_data_length = 4;  // Dictates maximum size comported by the sink

  assert(readable_block.CheckBounds(sizeof(SourceDataType)));
  SourceDataType *bl = readable_block.Scan<SourceDataType>();

  BxData<SinkDataType, 2 * source_data_length> bx_data;
  uint32_t count_m1 = 0, count_m2 = 0;

  for (unsigned int i = 0; i < source_data_length; i++) {
    const auto pt1 = uint32_t{(bl->mu1f[i] >> shifts::pt) & masks::pt};
    const auto pt2 = uint32_t{(bl->mu2f[i] >> shifts::pt) & masks::pt};

    // mu.extra is a copy of bl->bx with a change to the first bit.
    if (((pt1 > 0) || (doZS == 0))) {
      // set bit0 to "0" for first muon
      bx_data.Add({bl->mu1f[i], bl->mu1s[i], bl->bx[i] &= ~0x1});
      count_m1 += 1;
    }
    if (((pt2 > 0) || (doZS == 0))) {
      // set bit0 to "1" for second muon
      bx_data.Add({bl->mu2f[i], bl->mu2s[i], bl->bx[i] |= 0x1});
      count_m2 += 1;
    }
  }

  if (bx_data.count == 0) {
    LOG(WARNING) << '#' << nbPackets
                 << ": Detected a bx with zero muons, this should not "
                    "happen. Packet is skipped.";
    return -1;
  }

  LOG(DEBUG) << "Filling data with total size = " << std::to_string(bx_data.GetSize())
             << ", data points = " << std::to_string(bx_data.count)
             << ", available space = " << std::to_string(writeable_block.GetAvailable());

  // header word of packed muon data contains number of muons in words 3-4 and number of muons in
  // words 5-6, as well as the warning test enable flag.
  meta.header = uint32_t{(count_m1 << 16) + ((static_cast<uint32_t>(meta.header)) << 8) + count_m2};

  writeable_block.Fill(BxMetadata{meta.header, meta.bx, meta.orbit});
  writeable_block.Fill(bx_data.GetBuffer(), bx_data.GetSize());

  return bx_data.count;
}
