#! /usr/bin/python2

import socket
import time

MAX_RUNS = 3
EXEC_TIME_SECS = 5
STOP_DELAY_SECS = 10


def send_command(sock, cmd, run_no):
    message = cmd + " " + str(run_no)
    print "Sending message: ", message
    try:
        sock.sendall(message)
        data = sock.recv(256)
        print "Server response: ", data
        if "ok" in data:
            print "Command successful"
            return True
        else:
            return False
    except Exception as e:
        print "ERROR! ", e
        return False


if __name__ == "__main__":
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = ('localhost', 8000)
    sock.connect(server_address)

    do_start = True
    for current_run in range(0, MAX_RUNS):
        if do_start:
            print "Starting run ", current_run
            send_command(sock, "start", current_run)
            time.sleep(EXEC_TIME_SECS)

        print "Halting run ", current_run
        send_command(sock, "stop", current_run)
        time.sleep(STOP_DELAY_SECS)

    sock.close()
