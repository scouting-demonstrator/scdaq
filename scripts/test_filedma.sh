#!/bin/bash

## File: test_filedma.sh
## Author: @glazzari
## Description: runs file-based regression tests for GMT, BMTF, Calo and Passthrough processors.
## Style: Google Shell Guide (https://google.github.io/styleguide/shellguide.html).
## Gold standard for regression: scdaq @ adcd94aab4077367c6e2318589533e689f5fe2fe from May 29th, 2024.

readonly DATA_DIR=test/data
readonly CONFIG_DIR=test/config
readonly HASH_DIR=${DATA_DIR}/.sha256
readonly BUILD_DIR=build
readonly SCDAQ_TARGET=scdaq
readonly SCDAQ_PATH=${BUILD_DIR}/${SCDAQ_TARGET}
readonly MAX_RUNTIME_SECS=20
readonly MAX_HASHABLE_BYTES=104857600
readonly REF_COMMIT_TAG=adcd94aa # VCU128 processors

function build_scdaq() {
  mkdir -p ${BUILD_DIR}
  cd ${BUILD_DIR} && cmake .. && make ${SCDAQ_TARGET} && cd .. && echo "${FUNCNAME}: SUCCESS"
}

function cleanup_test() {
  cd ${DATA_DIR}
  rm -f *.dat *.raw* *.journal
  rm -rf in_progress run000*
  cd - > /dev/null && echo "${FUNCNAME}: SUCCESS"
}

function setup_test() {
  cleanup_test
  echo "${FUNCNAME}: SUCCESS"
}

function hash_first_n_bytes() {
  local num_bytes=$1
  local file_path=$2
  echo "$(head -c ${num_bytes} ${file_path} | sha256sum | cut -f1 -d' ' | cat)"
}

function check_hashes() {
  local num_bytes=$1
  local output_file=$2
  local reference_hash_file=$3

  output_hash=$(hash_first_n_bytes ${num_bytes} ${output_file})
  echo "${FUNCNAME}: computed hash is '${output_hash}'"

  if [ "$(cat ${reference_hash_file})" == "${output_hash}" ]; then
    echo "${FUNCNAME}: hashes match - SUCCESS"
    return 0
  else
    echo "${FUNCNAME}: hashes do not match!"
    echo "-- before changes: $(cat ${reference_hash_file})"
    echo "-- after changes:  ${output_hash}"
    return 1
  fi
}

function run_filedma_test() {
  local test_id=$1
  declare -i timeout_secs=$2
  local config_file=$3

  local expects_output=${EXPECTS_OUTPUT}
  local validates_contents=${VALIDATE_CONTENT}

  declare -i bytes_to_hash
  if [[ "${test_id}" == "BMTF_STUB" ]]; then
      bytes_to_hash=1048576
  else
      bytes_to_hash=${MAX_HASHABLE_BYTES}
  fi
  local ref_hash_file=${HASH_DIR}/${test_id}_${bytes_to_hash}_${REF_COMMIT_TAG}

  readonly test_id timeout_secs num_streams config_file expects_output ref_hash_file bytes_to_hash
  local output_dir output_prefix output_file ret_status

  setup_test

  output_dir=$(awk -F ':' '/^ *output_filename_base:/{printf $2}' ${config_file} | tr -d "\"',[:blank:]")
  output_prefix=$(awk -F ':' '/^ *output_filename_prefix:/{printf $2}' ${config_file} | tr -d "\"',[:blank:]")
  output_suffix=$(awk -F ':' '/^ *output_filename_suffix:/{printf $2}' ${config_file} | tr -d "\"',[:blank:]")

  local test_failed=1
  echo "${FUNCNAME}: starting SCDAQ test with ${test_id} P5 capture file"
  echo "${FUNCNAME}: timeout ${timeout_secs}s ./${SCDAQ_PATH} --config ${config_file}"
  timeout ${timeout_secs}s ./"${SCDAQ_PATH}" --config ${config_file} | uniq -uc

  ret_status="${PIPESTATUS[0]}"

  # We expect a TIMEOUT failure because `scdaq` is not normally supposed to return.
  if [[ "${ret_status}" -ne 124 ]]; then
    echo "${FUNCNAME}: SCDAQ test for ${test_id} FAILED with exit code ${ret_status}!"
  # Check if output file was correctly generated
  elif [[ ${expects_output} = true || ${validates_contents} = true ]]; then
    # Find file matching name pattern under the output directory, and excluding input data files
    # In case of multiple files, sorts alphanumerically so the lower-numbered one comes first
    local pattern=".*${output_prefix}.*${output_suffix}[_0-9]*"
    echo "${FUNCNAME}: searching for filename matching pattern: '${pattern}'"
    local output_files=$(find ${output_dir} -type f -iregex "${pattern}" ! -iname "*testfile.dat" | sort -rV)
    if [ -z "${output_files}" ]; then
      echo "${FUNCNAME}: SCDAQ test for ${test_id} FAILED: output file not found!"
      test_failed=1
    else
      echo "${FUNCNAME}: output file(s) found: ${output_files}."
      # More than one file may have been found; check by counting lines.
      local output_file
      if [[ $(echo "${output_files}" | wc -l) -gt 1 ]]; then
        # Since the list has been sorted, take the first (and "earliest") one with `head`
        output_file=$(echo "${output_files}" | head -n 1)
        stat $output_file
      else
        # Single matched file
        output_file="${output_files}"
      fi
      if [[ ${validates_contents} = false ]]; then
        test_failed=0
      else
        echo "${FUNCNAME}: taking file for comparison: ${output_file}."
        # Hash-verify contents
        if check_hashes ${bytes_to_hash} ${output_file} ${ref_hash_file} ; then
          echo "${FUNCNAME}: SCDAQ test PASSED for ${test_id}"
          test_failed=0
        else
          echo "${FUNCNAME}: SCDAQ test for ${test_id} FAILED"
        fi
      fi
    fi
  else
    # Execution ended by timeout, and not checking for output files
    test_failed=0
  fi
  return ${test_failed}
}

function echo_bool () {
  var=$1
  if [[ -z $var || $var -eq 1 ]]; then
    echo "X"
  else
    echo "Ü"
  fi
}

function run_all_tests () {
  local test_description=$1
  local expects_output=${EXPECTS_OUTPUT}
  local validates_contents=${VALIDATE_CONTENT}
  declare -i timeout_secs=${MAX_RUNTIME_SECS}
  readonly test_description timeout_secs expects_output validates_contents
  local gmt_failed=1 calo_tau_failed=1 calo_jet_failed=1 calo_egamma_failed=1 calo_sum_failed=1 bmtf_stub_failed=1
  local passthrough_failed=1 multistream_failed=1

  echo "${FUNCNAME}: running ${test_description}"

  failure_statuses=(gmt_failed calo_egamma_failed calo_jet_failed calo_tau_failed calo_sum_failed passthrough_failed)

  # The first argument must match the primitive identifier in the name of the file containing the hashes.
  run_filedma_test "GMT_MUON" ${timeout_secs} "${CONFIG_DIR}/filedma-gmt-muon.json5"
  gmt_failed=$?
  run_filedma_test "BMTF_STUB" ${timeout_secs} "${CONFIG_DIR}/filedma-bmtf-stub.json5"
  bmtf_stub_failed=$?
  run_filedma_test "CALO_TAU" ${timeout_secs} "${CONFIG_DIR}/filedma-calo-tau.json5"
  calo_tau_failed=$?
  run_filedma_test "CALO_JET" ${timeout_secs} "${CONFIG_DIR}/filedma-calo-jet.json5"
  calo_jet_failed=$?
  run_filedma_test "CALO_EGAMMA" ${timeout_secs} "${CONFIG_DIR}/filedma-calo-egamma.json5"
  calo_egamma_failed=$?
  run_filedma_test "CALO_SUM" ${timeout_secs} "${CONFIG_DIR}/filedma-calo-sum.json5"
  calo_sum_failed=$?
  run_filedma_test "PASS_CALO" ${timeout_secs} "${CONFIG_DIR}/filedma-passthrough.json5"
  passthrough_failed=$?

  if [[ ${validates_contents} = true ]]; then
    failure_statuses+=(multistream_failed)
    # We evaluate multi-stream operation with four parallel pipelines, but don't check hashes at this time
    VALIDATE_CONTENT=false run_filedma_test "MULTI_PASS" ${timeout_secs} "${CONFIG_DIR}/n-filedma.json5"
    multistream_failed=$?
  fi

  declare -i some_failed=0
  for status in "${failure_statuses[@]}"; do
      if [[ ! ${!status} =~ ^[01]$ ]]; then
        echo "${FUNCNAME}: ${test_description} ===> ERROR: Invalid return value for $status"
        some_failed=1
      fi
      if (( $status != 0 )); then
        echo "${FUNCNAME}: ${test_description} ===> FAILED ($status)!"
        some_failed=1
      fi
  done

  echo "${FUNCNAME}: error report for ${test_description} (Ü = SÜCCESS)"
  echo "-- GMT MUON        :  $(echo_bool ${gmt_failed} )"
  echo "-- BMTF STUB       :  $(echo_bool ${bmtf_stub_failed})"
  echo "-- CaloObj TAU     :  $(echo_bool ${calo_tau_failed})"
  echo "-- CaloObj JET     :  $(echo_bool ${calo_jet_failed})"
  echo "-- CaloObj EGAMMA  :  $(echo_bool ${calo_egamma_failed})"
  echo "-- CaloSum         :  $(echo_bool ${calo_sum_failed})"
  echo "-- Passthrough     :  $(echo_bool ${passthrough_failed})"
  if [[ ${validates_contents} = true ]]; then
    echo "-- Multi-stream:  $(echo_bool ${multistream_failed}) (=> files exist)"
  fi

  if (( $some_failed == 0 )); then
    echo "${FUNCNAME}: ${test_description} ===> SUCCESS"
  fi
  return $some_failed
}

function exit_on_failure {
    if [[ $? -ne 0 ]]; then
      exit 1
    fi
}

function main {
  echo "${FUNCNAME}: building scdaq locally"

  build_scdaq

  # Test 1: time-bound operation, checks for crashes
  EXPECTS_OUTPUT=false VALIDATE_CONTENT=false run_all_tests "Test 1 (operation) - ${MAX_RUNTIME_SECS}-second runs, no output files"
  exit_on_failure $?

  # Test 2: validates file output for basic runs
  EXPECTS_OUTPUT=true VALIDATE_CONTENT=true run_all_tests "Test 2 (regression) - ${MAX_RUNTIME_SECS}-second runs, check output files"
  exit_on_failure $?

  echo "${FUNCNAME}: all tests finished"
}

main "$@"