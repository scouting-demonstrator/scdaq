#!/bin/sh
#
# This script will start Scouting Data-Taking Software
#

echo "Clearing caches..."
sync
echo 3 | sudo tee /proc/sys/vm/drop_caches > /dev/null

umask 000 # Files and folders we create should be world accessible

while true 
do
    echo "Starting scdaq..."
    /opt/scdaq/bin/scdaq --config /etc/scdaq/scdaq.json5 2>&1
    if [[ ${PIPESTATUS[0]} == 78 ]]; then # Numerical value of EX_CONFIG.
      # If the configuration is incomplete there's no point in us retrying forever..
      exit 1
    fi
    echo "Clearing caches..."
    sync
    echo 3 | sudo tee /proc/sys/vm/drop_caches > /dev/null
    echo "Waiting 30 seconds..."
    for ((i=30; i>0; i=i-15)) 
    do
	echo "$i..."
	sleep 15
    done
done
