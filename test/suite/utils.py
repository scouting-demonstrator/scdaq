import logging
import shlex
import subprocess

# Adapted from https://stackoverflow.com/a/56944256
class ColorFormatter(logging.Formatter):
    colors = {
        "GREY": "\x1b[38;20m",
        "LIGHT BLUE": "\x1b[38;5;153m",
        "CYAN": "\x1b[36;1m",
        "GREEN": "\x1b[32;20m",
        "BOLD GREEN": "\x1b[32;1m",
        "YELLOW": "\x1b[33;20m",
        "ORANGE": "\x1b[38;5;208m",
        "RED": "\x1b[31;20m",
        "BOLD RED": "\x1b[31;1m",
    }

    reset = "\x1b[0m"

    def __init__(self, level_colors : dict, msg_format, date_format):
        self.msg_format = msg_format

        self.level_formats = {}
        for level, color in level_colors.items():
            self.level_formats[level] = self.colors.get(color, self.colors["GREY"]) + msg_format + self.reset
        super().__init__(msg_format, date_format)

    def format(self, record):
        # Create a combined string of filename, lineno, and funcName
        file_line = f"{record.filename}:{record.lineno}"
        record.codeInfo = f"{file_line:<20s} {record.funcName:>25s}()"
        # Get the appropriate log format for the level, or default to the base format
        log_fmt = self.level_formats.get(record.levelno, self.msg_format)
        formatter = logging.Formatter(log_fmt)
        return formatter.format(record)


class TestLogger(logging.Logger):
    SUCCESS_LEVEL_NUM = 100
    COMPLETE_LEVEL_NUM = 90
    date_fmt = '%Y-%m-%d %H:%M:%S'
    log_fmt = "%(asctime)s %(codeInfo)-48s %(levelname)8s: %(message)s"

    def __init__(self, name):
        super().__init__(name)
        logging.addLevelName(self.SUCCESS_LEVEL_NUM, "SUCCESS")
        logging.addLevelName(self.COMPLETE_LEVEL_NUM, "COMPLETE")

        self.level_colors = {
            self.SUCCESS_LEVEL_NUM: "BOLD GREEN",
            self.COMPLETE_LEVEL_NUM: "GREEN",
            logging.DEBUG: "YELLOW",
            logging.INFO: "LIGHT BLUE",
            logging.WARNING: "ORANGE",
            logging.ERROR: "RED",
            logging.CRITICAL: "BOLD RED"
        }

        # Create and configure the stream handler
        handler = logging.StreamHandler()
        handler.setLevel(logging.DEBUG)
        handler.setFormatter(ColorFormatter(self.level_colors, self.log_fmt, self.date_fmt))

        self.addHandler(handler)
        self.setLevel(logging.DEBUG)

    def success(self, message, *args, **kwargs):
        """Log a message with SUCCESS level."""
        if self.isEnabledFor(self.SUCCESS_LEVEL_NUM):
            self._log(self.SUCCESS_LEVEL_NUM, message, args, **kwargs)

    def complete(self, message, *args, **kwargs):
        """Log a message with COMPLETE level."""
        if self.isEnabledFor(self.COMPLETE_LEVEL_NUM):
            self._log(self.COMPLETE_LEVEL_NUM, message, args, **kwargs)


class CommandDispatcher:
    logger = TestLogger("CommandDispatcherLogger")

    @staticmethod
    def run(command, cwd=None, out=None, err=None):
        CommandDispatcher.logger.info(f"Executing command: {command}")
        # Use shlex to safely split commands whose arguments have spaces
        split_command = shlex.split(command)
        try:
            CommandDispatcher.logger.debug(f"Command split as: {split_command}")
            return subprocess.Popen(split_command, cwd=cwd, stdout=out, stderr=err or subprocess.STDOUT)
        except subprocess.CalledProcessError as e:
            CommandDispatcher.logger.error(f"Command failed: {e}")
            return None, str(e)
        except subprocess.TimeoutExpired as e:
            CommandDispatcher.logger.warning(f"Command timed out: {e}")
            return None, str(e)
        except Exception as e:
            CommandDispatcher.logger.error(f"Unexpected error: {e}")
            return None, str(e)
