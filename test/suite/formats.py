from enum import Enum

class EPrimitives(Enum):
    GMT_MUON = 1
    CALO_JET = 2
    CALO_TAU = 3
    CALO_SUM = 4
    CALO_EGAMMA = 5
    BMTF_STUB = 6
