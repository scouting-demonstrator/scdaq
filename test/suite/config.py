from pathlib import Path

import json5

from utils import TestLogger

logger = TestLogger("ConfigLogger")


class ScdaqConfig:
    def __init__(self, config_filepath: Path):
        self.path = None
        self.data = None
        self.load(config_filepath)

    @staticmethod
    def parse(config_file_path: Path):
        try:
            with open(config_file_path, 'r') as file:
                config_data = json5.load(file)
            return config_data
        except FileNotFoundError:
            logger.error(f"Error: Configuration file {config_file_path} not found.")
            return None
        except ValueError as e:
            logger.error(f"Error parsing JSON5 file: {e}")
            return None

    def load(self, config_file_path: Path):
        self.data = ScdaqConfig.parse(config_file_path)
        self.path = config_file_path

    def get(self, key: str, default_value=None):
        if key not in self.data and not default_value:
            logger.error(f"Error: key {key} not found in configuration.")
            raise KeyError
        return self.data.get(key, default_value)


class BuildConfig:
    """ Build-related configurations relevant only for setting up"""
    s_scdaqTargetName = "scdaq"  # Executable file stem
    s_buildType = "Debug"  # One of ["Release", "RelWithDebInfo", "Debug"]

    p_buildDir = Path("build")
    p_scdaqExecutable = p_buildDir / s_scdaqTargetName


class SuiteConfig:
    """ Test-specific parameters: file patterns, validation criteria """
    buildConfig = BuildConfig()

    p_configDir = Path("test/config")
    p_testArtifacts = Path("test/artifacts")

    k_defaultTimeoutSecs = 20


class EnvConfig:
    """ Configurations related to environment setup, e.g., directories, threads, ports, logging"""
    # Test paths and definitions
    p_baseRootDir = Path().resolve()
    s_scdaqLogName = 'scdaq'  # Log output file stem

    # A magic run number
    # NOTE: boolean used for compatibility with versions of SCDAQ that don't wait on the function manager
    # and instead start running immediately with run number 0
    # TODO: make first run number configurable on class initialization
    k_enableFnManager = False
    runNumber = 666505 if k_enableFnManager else 0

    def __init__(self, scdaq_config: ScdaqConfig):
        # Scdaq paths and definitions obtainable from json5 configs
        self.p_testDataDir = Path(scdaq_config.get("output_filename_base", ""))
        self.p_runDataDir = self.p_testDataDir / f"run{self.runNumber:0>6}"  # By convention

        self.fnManagerPort = int(scdaq_config.get("port"))
