from contextlib import contextmanager
from pathlib import Path

from validator import HashValidator
from utils import TestLogger
from data_manager import FileManager, ArtifactCollection
from controller import TestController
from config import EnvConfig, SuiteConfig

logger = TestLogger("ScdaqTestLogger")

class TestProfile:
    """ Definitions that are specific to a single test """
    def __init__(self, session_id, test_id, config_file: str, suite_config: SuiteConfig):
        self.s_sessionId = session_id
        self.s_testId = test_id
        self.configFile = config_file
        self.suiteConfig = suite_config

    def get_test_artifact_location(self):
        return self.suiteConfig.p_testArtifacts / f"{self.s_sessionId}/{self.s_testId}"


@contextmanager
def file_test_setup(file_manager: FileManager, test_prof: TestProfile, artifacts: ArtifactCollection):
    """ File-based context manager for setting up and cleaning up the test environment"""
    artifact_path = test_prof.get_test_artifact_location()
    try:
        file_manager.setup_test(test_prof.s_testId, artifact_path)
        yield
    finally:
        try:
            artifacts.save(artifact_path)
        except Exception as setup_exception:
            logger.error(f"Error during test wrap-up: {str(setup_exception)}")


class ScdaqTest:
    def __init__(self, env_config: EnvConfig, test_profile: TestProfile, validator: HashValidator):
        # Configuration
        self.testEnv: EnvConfig = env_config
        self.testProf: TestProfile = test_profile
        self.validator: HashValidator = validator

        # IO
        self.file_manager = FileManager(self.testEnv.p_testDataDir)
        self.artifacts = ArtifactCollection(self.testEnv.p_testDataDir)

    def run_test(self, timeout_secs):
        exe_path = self.testProf.suiteConfig.buildConfig.p_scdaqExecutable
        config_file = self.testProf.configFile
        scdaq_command = f"./{exe_path} --config {config_file}"
        controller = TestController(self.testEnv, scdaq_command, timeout_secs)

        with file_test_setup(self.file_manager, self.testProf, self.artifacts):
            # File in which scdaq outputs will be stored
            log_path = f"{self.testEnv.p_testDataDir}/{self.testEnv.s_scdaqLogName}.out"

            # Run scdaq
            controller.orchestrate_run(trigger_start=self.testEnv.k_enableFnManager, log_path=log_path)

            # Check results
            result = self.validator.check_outputs(self.testEnv.p_runDataDir)

            # Track artifacts of interest
            self.artifacts.track_path(path=Path(config_file), action=FileManager.copy)
            self.artifacts.track_path(path=Path(log_path), action=FileManager.move)
        return result
