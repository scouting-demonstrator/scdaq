import os
import shutil
from glob import glob
from pathlib import Path

from utils import TestLogger

logger = TestLogger("FileManagerLogger")


class FileManager:
    file_patterns_to_remove = ['*.out', '*.log', '*.err', '*.raw*', '*.journal']
    dir_patterns_to_remove = ["run*"]

    def __init__(self, root_path: Path):
        self.root_path = root_path

    @staticmethod
    def _handle_file_operation(operation, src, dst=None, operation_name: str = ""):
        try:
            if dst:
                operation(src, dst)
                logger.debug(f"{operation_name} {src} to {dst}")
            else:
                operation(src)
                logger.debug(f"{operation_name} {src}")
        except FileNotFoundError as e:
            logger.warning(f"File not found: {src}. Error: {str(e)}")
        except PermissionError as e:
            logger.error(f"Permission denied when {operation_name.lower()} {src}. Error: {str(e)}")
        except OSError as e:
            logger.error(f"OS error when {operation_name.lower()} {src}. Error: {str(e)}")

    @staticmethod
    def copy(src, dst):
        FileManager._handle_file_operation(shutil.copy, str(src), str(dst), "Copied")

    @staticmethod
    def move(src, dst):
        FileManager._handle_file_operation(shutil.move, str(src), str(dst), "Moved")

    @staticmethod
    def remove(src: Path):
        if src.is_dir():
            FileManager._handle_file_operation(shutil.rmtree, src, operation_name="Removed directory")
        else:
            FileManager._handle_file_operation(os.remove, src, operation_name="Removed file")

    @staticmethod
    def _truncate_operation(src: Path, dst: Path, bytes_to_truncate: int):
        """Internal file truncation logic"""
        with open(src, 'rb') as source_file, open(dst, 'wb') as dest_file:
            dest_file.write(source_file.read(bytes_to_truncate))

        # Check the size of the truncated file
        actual_size = os.path.getsize(dst)
        logger.debug(f"Truncated {bytes_to_truncate} bytes from {src} and saved to {dst}. "
                     f"Truncated file size: {actual_size} bytes.")

    @staticmethod
    def truncate(src: Path, dst: Path, bytes_to_truncate: int):
        # Use the existing `_handle_file_operation` to wrap the `_truncate_operation`
        FileManager._handle_file_operation(
            lambda s, d: FileManager._truncate_operation(s, d, bytes_to_truncate),
            src, dst, operation_name="Truncated"
        )

    @staticmethod
    def recursive_file_search(root: Path, file_pattern: str):
        """Find files based on the given patterns"""
        if not root.exists() or (file_pattern and not root.is_dir()):
            logger.error(f"Path {root} not found.")
            return []

        files = list(filter(os.path.isfile, glob(str(root) + "/**/" + file_pattern, recursive=True)))
        files.sort(key=os.path.getctime)
        logger.info(f"{len(files)} file(s) with '{file_pattern}' in {str(root)}.")
        return files

    @staticmethod
    def clean_up(root_path: Path, dir_patterns: list, file_patterns: list):
        removed_items = []
        for dir_pattern in dir_patterns:
            dir_paths = FileManager.recursive_file_search(root_path, dir_pattern)
            for dir_path in dir_paths:
                full_dir_path = Path(dir_path)
                if full_dir_path.is_dir() and full_dir_path != root_path:
                    removed_items.append(str(dir_path))
                    shutil.rmtree(dir_path, ignore_errors=True)

        for file_pattern in file_patterns:
            files = FileManager.recursive_file_search(root_path, file_pattern)
            for f in files:
                removed_items.append(str(f))
                file_path = Path(f)
                if file_path.exists():
                    file_path.unlink()

        logger.debug(f"Removed: {removed_items}")
        logger.complete("Clean-up done.")

    def setup_test(self, test_id: str, artifacts_path: Path) -> None:
        """Set up the test environment by cleaning up any leftover data."""
        FileManager.clean_up(self.root_path, self.dir_patterns_to_remove, self.file_patterns_to_remove)
        artifacts_path.mkdir(parents=True, exist_ok=True)
        logger.debug(f"Creating artifacts path {artifacts_path}.")
        logger.complete(f"Setup for {test_id}.")


class ArtifactCollection:
    l_trackedFilePatterns = ['*.out', '*.log', '*.err', '*.trunc*.raw']

    def __init__(self, root_src: Path):
        self.d_trackedItems: dict = {}
        self.rootSrc: Path = root_src

    def track_path(self, path: Path, pattern="", action=FileManager.move):
        if path.is_file():
            files = [path]
        else:
            files = FileManager.recursive_file_search(path, pattern)

        for file in files:
            resolved_file = Path(file).resolve()
            if resolved_file not in self.d_trackedItems:
                self.d_trackedItems[resolved_file] = action
                logger.info(f"Now tracking artifact {str(resolved_file)}")
            else:
                logger.warning(f"Artifact {str(resolved_file)} is already tracked, skipping.")

    def save(self, root_dst: Path):
        for file_pattern in self.l_trackedFilePatterns:
            self.track_path(self.rootSrc, file_pattern)

        """Copy or move tracked files and directories to a given path"""
        for file, action in self.d_trackedItems.items():
            action(file, root_dst)

        self.d_trackedItems.clear()
        logger.complete(f"Test artifacts saved in {root_dst}")
