import hashlib
import os
from collections import defaultdict
from pathlib import Path

from config import ScdaqConfig
from data_manager import FileManager
from utils import TestLogger

logger = TestLogger("HashValidatorLogger")


class HashValidator:
    p_hashDir = Path("test/data/.sha256")
    # TODO: Make CLI-configurable
    s_referenceCommitTag = "adcd94aa"
    k_defaultHashableBytes = 104857600
    # Override the default number of bytes to hash in the BMTF case, as it generates sparser outputs
    d_hashableBytes = defaultdict(lambda: HashValidator.k_defaultHashableBytes, {
        "BMTF_STUB": 1048576,
    })

    def __init__(self, test_id: str, scdaq_config: ScdaqConfig):
        self.s_testId = test_id
        self.k_expectOutput = True
        self.k_validateContent = True

        self.p_outputDir, self.s_outputPattern = self.get_output_pattern(scdaq_config)

    @property
    def hashableBytes(self):
        return self.d_hashableBytes[self.s_testId]

    @property
    def p_referenceHashFile(self):
        return self.p_hashDir / f"{self.s_testId}_{self.hashableBytes}_{self.s_referenceCommitTag}"

    @staticmethod
    def get_output_pattern(scdaq_config: ScdaqConfig):
        output_base_dir: str = scdaq_config.get('output_filename_base', "")
        output_prefix: str = scdaq_config.get('output_filename_prefix')
        output_suffix: str = scdaq_config.get('output_filename_suffix')
        has_cmssw_headers: str = scdaq_config.get('cmsswHeaders')

        logger.debug(f"Output configs: directory path {output_base_dir}")
        logger.debug(f"Output configs: prefix {output_prefix}, suffix {output_suffix}, cmssw? {has_cmssw_headers}")

        # By convention
        if has_cmssw_headers.lower() == "yes":
            return Path(output_base_dir), f"*{output_suffix}"
        else:
            return Path(output_base_dir), f"{output_prefix}*{output_suffix}"

    def check_outputs(self, root_dir: Path):
        if not self.k_expectOutput and not self.k_validateContent:
            logger.warning("Skipping output check -- nothing to validate.")
            return True

        output_files = self.find_output_files(root_dir)
        if not output_files:
            return False

        if self.k_validateContent:
            # Validate only the first output file
            return self.validate_file_contents(output_files[0])
        return True

    def find_output_files(self, root_dir: Path):
        logger.info("Checking existence of output files.")
        output_files = FileManager.recursive_file_search(root_dir, self.s_outputPattern)

        if not output_files:
            logger.error(f"No output file found for {self.s_testId} in directory: {root_dir}")
            return []

        logger.info(f"Found {len(output_files)} output file(s).")
        return output_files

    def validate_file_contents(self, output_file):
        logger.info(f"Validating content for earliest file `{output_file}`.")

        # Truncate the first n bytes and save to a new file in the artifacts directory
        truncated_file = self.p_outputDir / f"{Path(output_file).stem}.trunc{self.hashableBytes}.raw"
        FileManager.truncate(output_file, truncated_file, self.hashableBytes)

        # Check result against 'ground truth' hash for this test
        if self.check_file_hashes(truncated_file):
            logger.success(f"Test {self.s_testId} passed.")
            return True
        else:
            logger.error(f"Test {self.s_testId} failed!")
            return False

    @staticmethod
    def compare_hashes(incoming: str, reference: str):
        if reference == incoming:
            logger.success(f"Hashes MATCHED ({reference})")
            return True
        else:
            logger.critical("FAILURE! Hashes do not match:")
            logger.critical(f"-Reference: {reference}")
            logger.critical(f"-Incoming : {incoming}")
            return False

    @staticmethod
    def hash_n_bytes(file, num_bytes):
        """Hash the next `num_bytes` of the given open file."""
        return hashlib.sha256(file.read(num_bytes)).hexdigest()

    def check_file_hashes(self, output_file):
        """Check if the hash of the output file matches the reference hash"""
        # Read reference hash off pre-existing file
        with open(self.p_referenceHashFile, 'r') as ref_file:
            reference_hash = ref_file.read().strip()

        file_size = os.path.getsize(output_file)
        bytes_to_hash = min(self.hashableBytes, file_size)

        logger.info(f"Hashing {bytes_to_hash} bytes of file `{output_file}`.")
        if bytes_to_hash < self.hashableBytes:
            logger.error(f"File is too small to hash all {self.hashableBytes} bytes.")
            return False

        with open(output_file, "rb") as f:
            output_hash = HashValidator.hash_n_bytes(f, bytes_to_hash)

        logger.info(f"Hash comparison: `{output_file}` vs. `{self.p_referenceHashFile}` ")
        return self.compare_hashes(output_hash, reference_hash)
