#! /usr/bin/python3

import socket
import time
import argparse

from utils import TestLogger


class MockFunctionManager(object):
    logger = TestLogger("FunctionManagerLogger")
    def __init__(self, port: int=12000):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_address = ('localhost', port)
        self.sock.connect(self.server_address)
        MockFunctionManager.logger.complete(f"Connection set up at address {self.server_address}")

    def send_command(self, cmd, run_no):
        message = cmd + " " + str(run_no)
        MockFunctionManager.logger.info(f"Sending message: '{message}'")
        try:
            self.sock.sendall(message.encode('utf-8'))
            data = self.sock.recv(256).decode('utf-8')
            return "ok" in data
        except Exception as e:
            MockFunctionManager.logger.error(f"Exception: {e}! ")
            return False


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Pretend-FM')
    parser.add_argument('command', help='"start" or "stop"')
    parser.add_argument('number', type=int, help='run number', default=666666)
    parser.add_argument('--port', type=int, help='Port number (default=12000)', default=12000)
    parser.add_argument("--wait", type=int, help='Wait [msecs] before sending commands', default=0)

    args = parser.parse_args()
    fm = MockFunctionManager(args.port)

    current_run = args.number
    time.sleep(float(args.wait) / 1000.0)
    print(f"Command {args.command}, run {current_run}")
    if fm.send_command(args.command, current_run):
        print("Command successful")
    else:
        print("Command failed")
    fm.sock.close()
