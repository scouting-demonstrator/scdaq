import subprocess
import threading
from sys import stdout
from time import sleep

from config import EnvConfig
from utils import TestLogger, CommandDispatcher
from mock_fm import MockFunctionManager


class TestController:
    logger = TestLogger("TestControllerLogger")
    dispatcher = CommandDispatcher()

    def __init__(self, env_config: EnvConfig, scdaq_command, timeout_secs):
        self.env_config = env_config
        self.scdaq_command = scdaq_command
        self.timeout_secs = timeout_secs
        self.command_latency_secs = 2
        self.scdaq_proc = None

    def run_scdaq(self, log_stream=stdout):
        # Run SCDAQ and listen for `self.timeout_secs` seconds
        self.scdaq_proc = self.dispatcher.run(self.scdaq_command, out=log_stream)
        try:
            self.scdaq_proc.wait(timeout=self.timeout_secs)
        except subprocess.TimeoutExpired:
            self.logger.info(f"SCDAQ process ran until the time limit of {self.timeout_secs} seconds.")
            self.scdaq_proc.kill()

    def run_function_manager(self):
        fn_manager = MockFunctionManager(self.env_config.fnManagerPort)
        sleep(self.command_latency_secs)
        fn_manager.send_command("start", self.env_config.runNumber)
        # Sleep thread for less time than the maximum timeout scheduled
        runtime_secs = max(self.timeout_secs - self.command_latency_secs, self.command_latency_secs)
        sleep(runtime_secs)
        fn_manager.send_command("stop", self.env_config.runNumber)

    @staticmethod
    def run_in_thread(target, name, *args, **kwargs):
        daemon = kwargs.pop('daemon', False)  # Extract 'daemon' from kwargs (default False if not provided)
        TestController.logger.info(f"Starting thread {name}.")
        thread = threading.Thread(target=target, name=name, daemon=daemon, args=args, kwargs=kwargs)
        thread.start()
        return thread

    def orchestrate_run(self, log_path=None, trigger_start=True):
        log_stream = open(log_path, 'w') if log_path else stdout
        try:
            scdaq_thread = self.run_in_thread(self.run_scdaq, "SCDAQ", log_stream=log_stream)

            if trigger_start:
                # SCDAQ is waiting on a cue from the function manager
                sleep(1.5)  # Ensure SCDAQ is running
                self.run_in_thread(self.run_function_manager, "FunctionManager", daemon=True)

            scdaq_thread.join()
        finally:
            if log_path:
                log_stream.close()
