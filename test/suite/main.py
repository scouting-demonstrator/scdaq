import sys

from datetime import datetime
from contextlib import contextmanager

from formats import EPrimitives
from data_manager import FileManager
from utils import CommandDispatcher, TestLogger
from config import ScdaqConfig, SuiteConfig, EnvConfig
from test import ScdaqTest, TestProfile
from validator import HashValidator


class BaseTestSuite(object):
    logger = TestLogger("TestSuiteLogger")
    suiteConfig = SuiteConfig()

    @classmethod
    def reset(cls):
        cls.logger.complete("Cleaned up for regression test.")

    @classmethod
    def build_scdaq(cls):
        bld = cls.suiteConfig.buildConfig
        bld_dir = bld.p_buildDir
        logger = cls.logger

        if bld.p_scdaqExecutable.exists():
            logger.info(f"Using pre-built SCDAQ executable at {bld.p_scdaqExecutable}.")
            return True

        logger.info(f"Building SCDAQ from scratch in {bld_dir}...")
        bld_dir.mkdir(parents=True, exist_ok=True)

        dispatcher = CommandDispatcher()
        with open(f"{bld_dir}/build_scdaq.out", "w+") as fout:
            for command in [f"cmake -DCMAKE_BUILD_TYPE={bld.s_buildType} ..", "make"]:
                process = dispatcher.run(command, cwd=bld_dir, out=fout, err=sys.stderr)
                out, err = process.communicate()
                if process.returncode != 0:
                    logger.error(
                        f"Command `{command}` failed with exit code {process.returncode}: {err.decode('utf-8')}")
                    return False

        logger.complete("SCDAQ was successfully built.")
        return True


class FileTestSuite(BaseTestSuite):
    # TODO: Coverage grid is not yet implemented
    coverage_grid = {
        "with_cmssw_headers": False,
        "without_cmssw_headers": True,
        "base_primitives": [EPrimitives.CALO_JET, EPrimitives.GMT_MUON],
        "check_contents": True,
    }

    def __init__(self):
        dt = datetime.now()
        self.session_id = dt.strftime("%Y%m%d_%H%M%S")

    @classmethod
    def reset(cls):
        FileManager.remove(cls.suiteConfig.p_testArtifacts)
        super().reset()

    def run_all_tests(self):
        test_definitions = [
            {"test_id": "GMT_MUON", "config_file": "filedma-gmt-muon.json5"},
            {"test_id": "CALO_TAU", "config_file": "filedma-calo-tau.json5"},
            {"test_id": "CALO_JET", "config_file": "filedma-calo-jet.json5"},
            {"test_id": "CALO_SUM", "config_file": "filedma-calo-sum.json5"},
            {"test_id": "CALO_EGAMMA", "config_file": "filedma-calo-egamma.json5"},
            {"test_id": "BMTF_STUB", "config_file": "filedma-bmtf-stub.json5", "timeout": 30}
        ]

        # cmssw_tests = {
        #     "GMT_MUON_CMSSW": ("filedma-gmt-muon-cmssw.json5"),
        #     "CALO_TAU_CMSSW": ("filedma-calo-tau-cmssw.json5"),
        #     "CALO_JET_CMSSW": ("filedma-calo-jet-cmssw.json5"),
        #     "CALO_SUM_CMSSW": ("filedma-calo-sum-cmssw.json5"),
        #     "CALO_EGAMMA_CMSSW": ("filedma-calo-egamma-cmssw.json5"),
        #     "BMTF_STUB_CMSSW": ("filedma-bmtf-stub-cmssw.json5"),
        # }

        test_results = []
        for test_definition in test_definitions:
            test_id = test_definition["test_id"]
            config_file_path = self.suiteConfig.p_configDir / test_definition["config_file"]
            scdaq_config = ScdaqConfig(config_file_path)

            test_validator = HashValidator(test_id, scdaq_config)
            test_profile = TestProfile(self.session_id, test_id, config_file_path, self.suiteConfig)

            test = ScdaqTest(EnvConfig(scdaq_config), test_profile, test_validator)

            # Use default timeout if not specified
            timeout = test_definition.get("timeout", self.suiteConfig.k_defaultTimeoutSecs)

            # Run test
            success = test.run_test(timeout)
            if not success:
                self.logger.error(f"{test_id} test FAILED.")
                test_results.append(success)

        return all(test_results)


@contextmanager
def scdaq_run(test_suite: BaseTestSuite):
    try:
        test_suite.build_scdaq()
        test_suite.reset()
        yield
    except Exception as build_exception:
        test_suite.logger.error(f"Error during test wrap-up: {str(build_exception)}")
        raise


if __name__ == "__main__":
    suite = FileTestSuite()
    try:
        with scdaq_run(suite):
            if not suite.run_all_tests():
                suite.logger.error("One or more tests failed.")
                sys.exit(1)
            else:
                suite.logger.success("All tests passed!")
                sys.exit(0)
    except Exception as main_exception:
        print(f"Fatal error caused by exception: {str(main_exception)}")
        sys.exit(1)
